
local lg = love.graphics
local lm = love.mouse

-- buttons
Button={}
Button.__index = Button
Bar={}
Bar.__index = Bar

function math.round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end
---
function lg.rect(m,rect)
	return lg.rectangle(m,rect.x,rect.y,rect.w,rect.h)
end

function lg.drawR(res,rect)
	return lg.draw(res,rect.x,rect.y)
end

function Bar.newBar(rect,t,min,max,p)
	local  b = {}
	b.rect = rect
	b.text = t or ""
	b.show = true
	b.curVal = 0
	b.per = 0 or p
	b.min = min or 0
	b.max = max or 100
	return setmetatable(b,Bar)
	-- body
end

function Button.newButton(rect,t,i,func)
	local b={}
	b.rect = rect
	b.txt = t or ""
	b.img = i
	b.func = func
	b.show = true
	b.focus = false	
	b.clicked = false
	b.fixed = false
	return setmetatable(b,Button)
end

function Button:draw()
		
		local b = self
		local r = b.rect
		if(not b.show) then return end

		if(b.focus or b.clicked) then
			lg.setColor(50,100,100,255)
			if(b.fixed and b.clicked) then lg.setColor(150,150,50) end	
			if(b.img) then lg.setColor(255,255,255) end
		else
			lg.setColor(50,100,100,230)
		end
		
		
		
		if(b.img) then
			lg.draw(b.img,r.x,r.y)
		else
			lg.rect("fill",r)
		end
		
		local font=lg.getFont()		
		lg.setColor(255,255,255)
		lg.print(b.txt,r.x+(r.w-font:getWidth(b.txt))*0.5,r.y+12)
end

function Bar:draw()
	
	local b = self
	local r = b.rect

	if(not b.show) then return end
	
	lg.setColor(50,100,100,255)
	lg.rect("fill",r)
	

	local rr = newRect(r.x+8,r.y+r.h/2-2,r.w-16,4)
	lg.setColor(0,0,200)
	lg.rect("fill",rr)

	local mr = newRect(r.x+8+(r.w-16)*b.curVal-4,r.y+r.h/2-4,8)
	lg.setColor(0,200,200)
	lg.rect("fill",mr)

	local font=lg.getFont()		
	lg.setColor(255,255,255)

	local font = lg.getFont()
	--lg.setFont(font)
	

	lg.print(b.text,r.x+(r.w-font:getWidth(b.text))*0.5,r.y-font:getHeight())
	local  pr = math.round(b.curVal*(b.max-b.min),b.per)
	
	lg.print("- "..tostring(pr).." -",r.x+(r.w-font:getWidth("- "..tostring(pr).." -"))*0.5,r.y+r.h)

	lg.print(tostring(b.min),r.x,r.y+r.h)
	lg.print(tostring(b.max),r.x+r.w-font:getWidth(tostring(b.max)),r.y+r.h)

end

function Button:click(x,y)
	local b=self	
	if(not b.show) then return end
	
	if(pOnRect(b.rect,x,y) or b.focus) then
		b.clicked = not b.clicked
		if(b.func) then
			b.func()
		end
		return true
	end
	return false
end

function Bar:drag(x,y,c)
	--local c = c or  true
	local b = self
	local r = newRect(b.rect.x,b.rect.y,b.rect.w,b.rect.h)
	r.x=r.x+8
	r.w=r.w-16
	if(pOnRect(r,x,y) and b.show) then
		if(c) then
			b.curVal = (x-r.x)/(r.w)
			
			return math.round(b.min+(b.max-b.min)*b.curVal,b.per)
		end
	end	
	return false
end

function Button:update(dt)
	local x,y = lm.getPosition()
	local b = self
	b.focus = pOnRect(b.rect,x,y) and b.show
	if(not b.fixed) then b.clicked = false end
end
--	
function textBox(tlt,x,y,w,h)--moving from main.lua
	
	lg.setFont(fonts.menu)
	local h = h or fonts.menu:getHeight()+2
	local r = newRect(x-w*0.5,y,w,h)

	if((enableInput and lk.hasTextInput()==false) or 
	(pOnRect(r,mxp,myp) and lm.isDown("l"))) then
		enableInput = false 
		lk.setTextInput(true)
	end
	
	lg.setColor(50,50,50,200)
	lg.rect("fill",r)
	local inp  = input
	if(lk.hasTextInput()) then inp=inp.."|" end
	lg.setColor(255,255,255)
	lg.print(tlt..inp,r.x,r.y-2)
	
end

--math function
function newRect(x,y,w,h)
	return {x=x,y=y,w=w,h=h or w}
end

function pOnRect(rect,x,y)
	return (x-rect.x<rect.w and x-rect.x>0 and y-rect.y<rect.h and y-rect.y>0)
end

