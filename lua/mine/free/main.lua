require("utils")

local lg = love.graphics
local lm = love.mouse
local rand = love.math.random
local lk = love.keyboard
local lf = love.filesystem

local getTime = love.timer.getTime

---
function lg.rect(m,rect)
	return lg.rectangle(m,rect.x,rect.y,rect.w,rect.h)
end

function lg.drawR(res,rect)
	return lg.draw(res,rect.x,rect.y)
end

--- long
local lang={
	newgame = "Новая",
	options = "Опции",
	exit = "Выход",
	winer = "Ты выиграл!",
	loser = "Ты проиграл(!",
	hightScore = "Ваш | Лучший",
	conf_counts = "Размер поля:",
	conf_mcount = "Количество мин:"
}

---
local SW,SH = lg.getWidth(),lg.getHeight()
local gVer = "0.4b PC/ANDROID"
--gameVar
local MAIN,GAME,WIN,GAMEOVER,OPTIONS = 0,1,2,3,4
local gMode = MAIN --/main  - main menu

local scores={}

--map vars
local mapW,mapH,mCount=14,14,32

local curentMS = 1
local gMSL={
	{14,14,16},
	{25,25,40},
	{40,40,100}
}

local findMines = 0
local flagAdds = mCount

local scaleMap= 1
local tWn,tHn=100,50--for scale

local tW,tH=100,50
local scrollX,scrollY=0,0
local gTimer=300
local mineOrFlag = 0

local map={}
local mMap={}

local imgs={}

--GUI vars
local gui={}
local fonts={}
local bts={}
----

-- LOAD--------------------------------------------------
function love.load()
	
	loadSettings()
	loadScores()
	--love.system.vibrate(3000)
	fonts={
		top = lg.newFont("font.ttf",64),
		menu = lg.newFont("font.ttf",32),
		micro = lg.newFont("font.ttf",20)
	}
	
	gui={
		flag = lg.newImage("tiles/gui/flag.png"),
		timer = lg.newImage("tiles/gui/timer.png"),
		menu = lg.newImage("tiles/gui/menu.png"),
		replay = lg.newImage("tiles/gui/replay.png"),
		flag_btn = lg.newImage("tiles/gui/flag_btn.png")
	}
	
	imgs={
		[1]=lg.newImage("tiles/sand2.png"),
		[2]=lg.newImage("tiles/tnt.png"),
		[3]=lg.newImage("tiles/flag2.png"),
		[4]=lg.newImage("tiles/0tile.png")
	}
	for i=1,8 do --without zero and 9 numbers 
		table.insert(imgs,lg.newImage("tiles/num/"..i..".png"))
	end
	
	lg.setDefaultFilter("nearest","nearest",8)

	
	--buttons
	local rs = newRect((SW-340)*0.5,(SH-300)*0.5,340,300)--spalsh
	
	local iw=gui.menu:getWidth()
	local r1 = newRect(rs.x+rs.w/2-iw-16,rs.y+rs.h-iw-16,iw)
	local r2 = newRect(rs.x+rs.w/2+16,rs.y+rs.h-iw-16,iw)

	---menu button size
	local rw = 300
	local rh = 64		

	bts={
		gui_menu=Button.newButton(newRect(2,0,64),nil,gui.menu,function() gMode=MAIN end),
		gui_flag=Button.newButton(newRect(SW-80,SH-80,64),nil,gui.flag_btn,nil),
		st_menu=Button.newButton(r1,nil,gui.menu,function() gMode=MAIN end),
		st_replay=Button.newButton(r2,nil,gui.replay,resetGame),
		main_new=Button.newButton(newRect((SW-rw)/2,(SH-70*3)/2,rw,rh),lang.newgame,nil,resetGame),
		main_opt=Button.newButton(newRect((SW-rw)/2,(SH-70*3)/2+(rh+4),rw,rh),lang.options,nil,nil),
		main_exit=Button.newButton(newRect((SW-rw)/2,(SH-70*3)/2+2*(rh+4),rw,rh),lang.exit,nil,love.event.quit),
		[10]=Button.newButton(newRect((SW-rw)/2,(SH-70*3)/2-(rh+4),(rw-20)/3,rh),"14x14",nil,function() curentMS=1 end),
		[20]=Button.newButton(newRect((SW-rw)/2+(rw+10)/3,(SH-70*3)/2-(rh+4),(rw-20)/3,rh),"20x20",nil,function() curentMS=2 end),
		[30]=Button.newButton(newRect((SW-rw)/2+(rw+10)/1.5,(SH-70*3)/2-(rh+4),(rw-20)/3,rh),"40x40",nil,function() curentMS=3 end)	
	}
	
	bts[10].fixed = true
	bts[20].fixed = true
	bts[30].fixed = true
	bts.gui_flag.fixed=true
	
	drags={
		map = Bar.newBar(newRect(SW/2-150,SH/4,300,25),lang.conf_counts),
		count = Bar.newBar(newRect(SW/2-150,SH/2,300,25),lang.conf_mcount)
	} 

	genMap()
	mapCentered()
	
end

--- END LOAD-------------------------------

local mxp,myp=0,0
local mi,mj=0,0
local draget=false --т¤гучее поле
local omxp,omyp=0,0

local fx,fy=0,0

local whell=0
--timer states
local gTimerStart = 0
local gTimerStep = 1
local gTimerTriger = false

function gTimerReset(t)
	gTimerTriger = true
	gTimer = 0
	gTimerStart = getTime()+ (t or 0) 
	
	if(t) then gTimerStep = -1 end
	
end

function gTimerUpdate()

	if(gTimerTriger) then
		gTimer = (getTime()-gTimerStart)*gTimerStep
		if(gTimer==0 and gTimerStep<0 ) then gTimerTrig = false end	
		return true
	end
	return false
end
---UPDATE----------------------------------------------------
function updButton()
	---hide or show buttons
	bts.gui_menu.show = (gMode==GAME) --hide gui btn
	bts.gui_flag.show = (gMode==GAME) --hide gui btn
	bts.st_menu.show = (gMode==GAMEOVER or gMode==WIN) --hide gui btn
	bts.st_replay.show = (gMode==GAMEOVER or gMode==WIN) --hide gui btn
	
	local m = (gMode==MAIN)
	bts.main_new.show = m --hide gui btn
	bts.main_opt.show = m --hide gui btn
	bts.main_exit.show = m --hide gui btn
	
	local o = (gMode==OPTIONS)
	drags.count.show = o
	drags.map.show = o

	for i=10,30,10 do
		bts[i].show = m --hide gui btn
		bts[i].clicked = false
	end
	bts[10*curentMS].clicked=true
	
	if(love.system.getOS()~="Android") then
		for i,v in pairs(bts) do v:update() end --upd button, 
		for i,v in pairs(drags) do v:drag(mxp,myp,lm.isDown("l")) end --dragBar
	end

end

function love.update()

	mxp,myp = lm.getPosition()

	updButton()--buttons
	gTimerUpdate()--timer
	
	
	updMap()-- upd map,used mxp and omx for dragin


	omxp,omyp=mxp,myp
end

function love.quit()
	saveSettings()

	return false
end
--file function

function split(str,toc)
	local toc =  toc or "[^%s]+"
	local parsed = {} --temp
	for p in str:gmatch(toc) do --spliting by "space"
		table.insert(parsed,p)
	end
	return parsed	
end

function addScore(t,m)
	local t,m = t,m

	local p = curentMS
	local pos = gMSL[p][1]*gMSL[p][2]*gMSL[p][3]
	if(scores[pos]) then
		if(scores[pos].time<t and scores[pos].time~=0 
		and scores[pos].mine>m and scores[pos].mine~=0 ) then 
			t = scores[pos].time 	
			m = scores[pos].mine 
		 end 	
	end
	scores[pos].time = t
	scores[pos].mine = m
end

function  saveScores()
	local  f = lf.newFile("scores.cfg")
	f:open("w")
	
	for k,v in pairs(gMSL) do
		local pos = v[1]*v[2]*v[3]
		local t,m = 0,0
		if(scores[pos]) then 
			t = scores[pos].time or 0
			m = scores[pos].mine or 0 
		end
		local str = string.format("%d %d %d\n",pos,t,m)
		f:write(str)
	end
	f:close()
end

function  loadScores()
	if(lf.exists("scores.cfg")) then
		local  f = lf.newFile("scores.cfg")
		f:open("r")
		for v in f:lines() do
			local p = split(v)
			scores[tonumber(p[1])]={
					time = tonumber(p[2]),
					mine = tonumber(p[3])	
				}
		end
		f:close()
	else
		saveScores()
		loadScores()
	end
end

function loadSettings()
	local sp = lf.getSaveDirectory()
	if(lf.exists("set.cfg")) then
		local f = lf.newFile("set.cfg")
		local st = f:lines()()--because interator
		local parsed = split(st)
		f:close()
		-- set to vars
		local pos = 1
		for k=0,#gMSL-1 do 
 	 		gMSL[k+1][1] = tonumber(parsed[3*k+1])
 	 		gMSL[k+1][2] = tonumber(parsed[3*k+2])
 	 		gMSL[k+1][3] = tonumber(parsed[3*k+3])
 	 		pos = pos + 3 
		end
		curentMS = tonumber(parsed[pos])
		flagAdds = gMSL[curentMS][3]
	else
		saveSettings()
	end
end

function saveSettings()
	local st = ""
	for k,v in pairs(gMSL) do 
 	 	st=st..string.format("%d %d %d ",v[1],v[2],v[3]) --save construction
	end
	st = st .. tostring(curentMS)
	local f = lf.newFile("set.cfg")
	f:open("w")
	local stats = f:write(st)
	f:close()
	return stats	
end

-- isometric math
function isoToScreen(i,j,k)
	local k=k or 0
	local x = scrollX + (i-j)*tW/2
	local y = scrollY + (i+j-k*2)*tH/2
	return x,y
end

function screenToIso(x,y)
	local f = math.floor
	local	i = (x-scrollX)/tW+(y-scrollY)/tH
	local	j = (y-scrollY)/tH-(x-scrollX)/tW
	return f(i),f(j)
end

function mapCentered()
	tW,tH=tWn*scaleMap,tHn*scaleMap
	local len = math.sqrt(mapH^2+mapW^2)
	scrollX = (SW-tW)/2
	scrollY = (SH-len*tH)*0.5+tH*2 
end

--map generation
function genMap(show)
	
	--bts[#bts].clicked=true
	mapH,mapW,mCount = unpack(gMSL[curentMS])
	flagAdds = mCount
	for i=1,mapW*(mapH+1) do --free map

		map[i]=0
		if(gMode==GAME) then
			mMap[i]=0 --gameMode
		else
			mMap[i]=-1
		end
	end
	
	local i=0
	
	while(i<mCount) do
		local x,y = math.floor(rand(1,mapW)),math.floor(rand(1,mapH))
		if(map[x*mapH+y]~=-1) then
			map[x*mapH+y]=-1
			i=i+1
		end
	end

	for i=1,mapW do
			for j=1,mapH do			    
					if(map[i*mapH+j]~=-1)then
							local ink=0
							for r=-1,1 do
									for t=-1,1 do
											local ir=i+r
											local jt=j+t
											
											if(ir>0 and jt>0 and 
												ir<mapW+1 and jt<mapH+1 
												and map[ir*mapH+jt]==-1) then
												ink=ink+1
											end
											
									end
							end
						map[i*mapH+j]=ink
					end
			end
	end
	
end

---map functions 
function trase(a,b,u)
	if(a<1 or a>mapW or b<1 or b>mapH)
	then
			return 
	end
	
	local u=u or false
	local m=map[a*mapH+b]
	local om=mMap[a*mapH+b]
	
	if(m~=-1  and om==0) then
			mMap[a*mapH+b]=-1
			if(m>0) then
				return
			else
			trase(a+1,b)
			trase(a+1,b+1)
			trase(a+1,b-1)
			trase(a,b-1)
			
			trase(a-1,b-1)
			trase(a-1,b+1)
			trase(a-1,b)
			trase(a,b+1)
			end
	end
end
--
function traseAll()
	for i=1,mapH*(mapW+1) do mMap[i]=-1 end
end

--update map function
local fromI,toI=1,mapW
local fromJ,toJ=1,mapH

function updMap()

	
	fromI,_=screenToIso(0,0)
	toI,_=screenToIso(SW+tW,SH+tH)
	dx,fromJ=screenToIso(SW+tW,0)
	_,toJ=screenToIso(0,SH+tH)

	if(fromI<1) then fromI=1 end
	if(fromJ<1) then fromJ=1 end
	if(toI>mapW) then toI=mapW end
	if(toJ>mapH) then toJ=mapH end

	coner={} -- map's coner's to screen
	coner.minx,_ = isoToScreen(1,mapH)
	_,coner.miny = isoToScreen(1,1)
	coner.maxx,_ = isoToScreen(mapW,1)
	_,coner.maxy = isoToScreen(mapW,mapW)
	coner.maxy = coner.maxy - SW
	coner.maxx = coner.maxx - SH

	if(gMode==GAME or gMode==GAMEOVER) then
	
	 if(love.system.getOS()~="Android") then
		local dx,dy=mxp-omxp,myp-omyp
		local abs = math.abs
		
		if(abs(dx)>5 or abs(dy)>5) then
				if(lm.isDown("l")) then
						draget=true
						--if(dx*coner.maxx<0 or dx*coner.minx<0) then --bug 
							scrollX=fx+mxp
						--end
						--if(dy*coner.maxy<0 or dy*coner.miny<0) then 
							scrollY=fy+myp
						--end					
				end
			--end
				
			end
		mi,mj = screenToIso(mxp-tW/2,myp)
		end
	end
	
end

function mapResize(op)
	scaleMap = op+scaleMap
	if(scaleMap>2) then scaleMap = 2 end
	if(scaleMap<0.1) then scaleMap = 0.1 end
	tW=tWn*scaleMap
	tH=tHn*scaleMap
end

--game function
function resetGame(t)
	if(gMode~=GAME) then
		flagAdds = mCount
		findMines=0
		gMode=GAME
		scaleMap=1
		genMap(false)
		gTimerReset(t)
		mapCentered()
	end
end

function gameState(st)
	gTimerTriger=false
	
	addScore(gTimer,findMines)
	saveScores()
	
	if(st==WIN) then
		gMode = WIN
	elseif(st==GAMEOVER) then
		--stopTimer
		traseAll()
		gMode = GAMEOVER
		if(love.system.vibrate) then 
			love.system.vibrate(0.3)
		end
	end
end

--input callback
local resize
function love.touchgestured(x,y,a,d)
		resize = true
		mapResize(d)
end

function love.touchmoved(id,x,y,i)
		local x=x*SW
		local y=y*SH
		
		for i,v in pairs(drags) do
				if(v:drag(mxp,myp,lm.isDown("l"))>0) then  return end
		end
		
		if(gMode~=GAME and gMode~=GAMEOVER or resize) then return end 
		
		scrollX=fx+x
		scrollY=fy+y
		draget=true
		
		mi,mj = screenToIso(x-tW/2,y)
	 
end

function love.mousepressed(x,y,key)

	--if(not bar:drag(x,y,key=="l") ) then return end
	if(gMode==MAIN) then
		return
	elseif(gMode==GAME or gMode==GAMEOVER) then
		whell=0
		if(key=="l") then --start scroll
			fx = scrollX-x
			fy = scrollY-y
		else
			if(key=="wu") then whell=1 end
			if(key=="wd") then whell=-1 end
			resize = true
			mapResize(0.1*whell*scaleMap*scaleMap)
		end
	end
	
end

function love.mousereleased(x,y,key,t)
	local key=key
	for i,v in pairs(bts) do --upd click of button
		if(v:click(x,y)) then 
				return --break click in map
	 	end
	end 
	
	if(t and bts.gui_flag.show) then--android swither
			if(bts.gui_flag.clicked) then
				key="r"
			end 
	end
	--
	if(draget or resize) then
		 draget=false; 
		 resize=false; 
		 return 
	end
	
	mi,mj = screenToIso(x-tW/2,y)
	
	local mi=mi+1
	local mj=mj+1
	if(mj<1 or mi<1 or mi>mapW or mj>mapH or gMode~=GAME) then return end
	
	local om=map[mi*mapH+mj]
	local mom=mMap[mi*mapH+mj]

	if(key=="r" and mom~=-1) then
	
		if(flagAdds>0) then 
			flagAdds=flagAdds-(1-mom*2)
			if(om==-1) then 
				findMines=findMines+(1-mom*2)
			end
			if(findMines==mCount) then gameState(WIN) end
			mom=math.abs(mom-1)
		else
			if(mom==1) then 
				mom=0
				flagAdds=1
			end
		end
	elseif(key=="l") then
	  if(mom~=1) then mom=-1 end
	  
		if(om==-1) then
		  gameState(GAMEOVER)
		else
		   trase(mi,mj)
		end
		
	end
	
	map[mi*mapH+mj]=om
	mMap[mi*mapH+mj]=mom

end

--main drawer
function love.draw()
	
	drawMap()
	if(gMode==MAIN) then
		drawMain()--delete it?
	elseif(gMode==OPTIONS) then
		--menu
	elseif(gMode==GAME) then
		drawGUI()
	else 
		drawST()
	end
	
	lg.setColor(255,0,0)
	lg.setFont(fonts.menu)

	for i,v in pairs(bts) do v:draw() end --draw button
	for i,v in pairs(drags) do v:draw() end --draw dragers

	lg.setFont(fonts.micro)
	lg.print("FPS:"..love.timer.getFPS(),0,SH-40)
	lg.print("Ver:"..gVer,0,SH-20)
	--lg.print(tostring(oldd),0,SH-80)
end

--draw functions
function drawST()
	--textBox("Имя:",SW*0.5,SH*.25,300)
	local rw = 340
	local rh = 300

	local r = newRect((SW-rw)*0.5,(SH-rh)*0.5,rw,rh)
	-- 
	lg.setColor(64,64,64,100)
	lg.rectangle("fill",0,0,SW,SH)
	--
	lg.setColor(200,200,255,180)
	lg.rect("fill",r)
	
	local txt =  lang.winer
	lg.setColor(0,128,32)
	--colorDialog()
	if(gMode==GAMEOVER) then
		txt = lang.loser
		lg.setColor(128,32,0)
	end
	
	lg.setFont(fonts.menu)
	lg.print(txt,(SW-fonts.menu:getWidth(txt))*0.5,r.y+16)
	--icon,buttons draw
	lg.setColor(255,255,255)
	
	lg.draw(gui.timer,r.x+20,r.y+64)
	lg.draw(gui.flag,r.x+20,r.y+64+80)
	
	lg.setColor(0,0,0)
	
	local  sp = gMSL[curentMS][1]*gMSL[curentMS][2]*gMSL[curentMS][3]
	
	--lg.print(lang.hightScore,r.x+120,r.y+64)
	local t = string.format("%02d:%02d | %02d:%02d",math.floor(gTimer/60),(gTimer%60),math.floor(scores[sp].time/60),(scores[sp].time%60))
	lg.print(t,r.x+100,r.y+64+12)
	t = string.format("%d/%d | %d/%d",findMines,mCount,scores[sp].mine,mCount)
	lg.print(t,r.x+100,r.y+64+12+80)
end

function drawMain()
	--all in buttons 
	--BG

	lg.setColor(64,64,64,100)
	lg.rectangle("fill",0,0,SW,SH)
	
end

function drawGUI()

	lg.setFont(fonts.top)
	lg.setColor(50,100,100,190)
	lg.rectangle("fill",0,0,SW,64)
	
	local txt = string.format("%03d|%02d:%02d",flagAdds,math.floor(gTimer/60),(gTimer%60))
	
	local ofs = fonts.top:getWidth(txt)
	lg.setColor(0,0,0)
	lg.print(txt,(SW-ofs)*0.5,-12)
	lg.setColor(255,255,255)
	lg.draw(gui.flag,(SW-ofs)*0.5 - gui.flag:getWidth(),0)
	lg.draw(gui.timer,(SW+ofs)*0.5+12,0)
	lg.setColor(255,255,255,120)
end


function drawMap()

	for j=fromJ,toJ do
		for i=fromI,toI do
			
			local obj=map[i*mapH+j]
			local sh=mMap[i*mapH+j]
			
			local ind=1
			local dZ=0
			local bg = false --for numbers
			---REFACTORING!!!!---
			if(sh==-1) then
			  if(obj==-1) then ind=2 end
			  if(obj==0) then ind=4 end
					if(obj>0 and obj<10) then 
							bg=true
							ind=4+obj
							dZ=0
					end
			else
			  if(sh==1) then
			  		ind=3
			  		bg=true
			  		dZ=1
			  end
			end
			
			if(i==mi+1 and j==mj+1) then 
				lg.setColor(255,180,180)
			elseif(sh==-1) then --light of z =)))
				lg.setColor(200,200,200)
				if(obj>0 and obj<10) then 
					lg.setColor(230,230,230)	
				end
				else
				lg.setColor(255,255,255)
			end
			
			local x,y=isoToScreen(i-1,j-1)
			if(bg) then	
				lg.draw(imgs[1],x,y,0,scaleMap,scaleMap)			
			end
			lg.draw(imgs[ind],x,y-dZ*tH,0,scaleMap,scaleMap)
			lg.setColor(255,0,0)
		end
	end	
end
