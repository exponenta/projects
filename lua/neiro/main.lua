-- your code goes here
--//neiro

local Neirons = {}
Neirons.__index=Neirons
local count = 0
function newNeiro(input,value,func,name,range,a)
	n={}
	n.range=range or 1
	n.a = a or 1
	n.input = input or {}
	n.value = value or 0
	n.func = func or sigma
	n.weight = {}
	math.randomseed (os.clock())
	for i=1,#n.input do
		n.weight[i]=math.random()*0.2+0.1
	end
	n.name = name or "neiro_"..count
	count = count+1
	return setmetatable(n,Neirons)
end

function Neirons:math()
	local n = 0
	if(#self.input>0) then
		for i=1,#self.input do 
			self.input[i]:math()--пересчитываем входной нейрон
			n = self.weight[i]*self.input[i].value+n
		end
		self.value = self.func(n,self.range,self.a) 
	end
end
--обновляем веса синапсов нейронов 
function Neirons:newWeights(d,req)
	for i=1,#self.input do
		if(req) then
			self.input[i]:newWeights(d*self.weight[i]) -- обновляем веса всех 
		end
		self.weight[i] = self.input[i].value*d + self.weight[i]
	end
end
--порог
function porog(n,range,a)
	if n > range then
		return 1
	else
		return 0
	end
end
--сигмоидная функция
function sigma(n,range,a)
	local range = range or 1
	local a = a or 1
	return range*(2/(1+math.exp(-n*a-1)))
end
--тупо линейная ф-ция 
function line(n,range,a)
	return n
end


local fn,sn=newNeiro(),newNeiro()
local hn1,hn2=newNeiro({fn,sn},0,line),newNeiro({fn,sn},0,line)
local add = newNeiro({hn1,hn2},0,line,nil,0.5,1)


local a_and={
{1,1,0},
{1,2,-1},
{3,4,-1},
{4,1,3},
}

for i=1,1000 do
	local gErr = 0

	for p=1,#a_and do
		fn.value = a_and[p][1]
		sn.value = a_and[p][2]
		add:math()

		local err = a_and[p][3] - add.value
		add:newWeights(err*0.05,true)
		gErr = math.abs(err)+gErr
	end

	if(gErr<0.00001) then
		print(string.format("Studied of interation %d",i))
	 	break 
	end
end

for p=1,10 do
		fn.value = math.random(0,10)
		sn.value = math.random(0,10)
		add:math()
		print(string.format("%f - %f = %f",fn.value,sn.value,add.value))
end