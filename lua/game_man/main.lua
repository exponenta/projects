require("libs.anal")
local joy=require("libs.joy")
local vector=require("libs.vector")
local camera=require("libs.camera")
local WC=require("libs.hardoncollider")()
local maper=require("libs.maper")
local state = require("libs.state")
local BLogic = require("libs.bots")
local sLight = require("libs.slight")

local lg=love.graphics
local lm=love.mouse
local lf=love.filesystem
local SW,SH=lg.getWidth(),lg.getHeight()
local DEBUG=true
local cam=camera.new(nil,nil,1,0)

local bots={}


local text={}
 
local index=1
local function addToLog(t)
	text[index]=t--string.format("collision:(%d,%d)",dx,dy)
	index=index+1	
	if(index>10) then index=1 end
end


local function on_coll(dt,sa,sb,dx,dy)
	sa.collided=true
	sb.collided=true
	
	if(sa.type=="bot" or sa.type=="player") then
		sa:move(dx,dy)
	end
	
	if(sb.type=="bot" or sb.type=="player") then
		sb:move(-dx,-dy)
	end

end

local function off_coll(dt,sa,sb)
	sa.collided=false
	sb.collided=false
end

local showJoy = true


function love.load()

	showJoy = love.system.getOS()=="Android"

	WC:setCallbacks(on_coll,off_coll)
	maper.getNewShape = getNewShape  
	
	mapObj = maper.loadMapFile("/maps/1.lua")
	mapTs = maper.getTileSet(mapObj,"/maps/")
	mapData = maper.getLayersData(mapObj,mapTs)

	man_img=lg.newImage("sprites/man_walk.png")
	man_anim=newAnimation(man_img,32,32,0.1,0)
	
	light_img = lg.newImage("sprites/light.png")
	light_box = lg.newCanvas(SW,SH)

	local pos=mapData.points["start"][1]

	player = WC:addCircle(pos.x,pos.y,18)
	player.anim=man_anim
	player.img=man_img
	player.update=false
	player.show=true
	player.debug=true
	player.dir=vector(1,0)
	player.type="player"
	bots.player=player


	addBots(mapData.points)
	
	light = sLight.addLight(200,360)
	sLight.setBoundsShape(mapData.colliders)

	light:move(player:center())

	joy.init()
	
end

function drawBounceMap(coll)
	local canvas = lg.newCanvas(32*32,32*32)
	local coll = coll
	canvas:clear()
	lg.setCanvas(canvas)
	--lg.setColor(0,0,0)
	for _,v in pairs(coll) do
		v:draw("fill")
	end
	lg.setCanvas()
	return canvas
end

local msx,msy=0,0

local mem=""
local t=0

local camMoveTo={x=100,y=100}

local playerX,playerY=0,0
local joyVect=vector(joy.x,joy.y)

local  isDown = love.keyboard.isDown

function love.update(dt)
	
	
	if(showJoy) then
		joy.update()
		joyVect.x=joy.x
		joyVect.y=joy.y
	else
		joyVect.x = 0
		joyVect.y = 0
		if(isDown("w") ) then joyVect.y=-1 end
		if(isDown("s") ) then joyVect.y=1 end
		if(isDown("d") ) then joyVect.x=1 end
		if(isDown("a") ) then joyVect.x=-1 end

		joyVect:normalize_inplace()
	end

	WC:update(dt)
	updateBots(dt)

	if(joyVect:len()>0.01) then

		player:move(math.floor(joyVect.x*200*dt),math.floor(joyVect.y*200*dt))
		man_anim:setFrames(1,4)
		player.dir=joyVect
		man_anim:update(dt*joyVect:len())	

		light:move(playerX,playerY)
	
	else
		man_anim:seek(1)		
	end

	playerX,playerY=player:center()

	local campx,campy=cam:cameraCoords(playerX,playerY)
	
	if ((campx>SW*0.75) or (campx<SW*0.25) 
					or (campy>SH*0.75) or (campy<SH*0.25)) then
			camMoveTo.x=playerX
			camMoveTo.y=playerY
	end
	
	local camPos=vector.moveTo({cam.x,cam.y},camMoveTo,5)
	cam:lookAt(math.floor(camPos.x),math.floor(camPos.y))

	
--memory used
	t=t-dt
	if t<0 then
		t=1
		mem=string.format("Lua MEM:%.3fKb",collectgarbage("count"))
	end
	
end

function updLight()

	lg.setCanvas(light_box)
	
	light_box:clear(0,0,0,255)
	lg.setColor(0,0,0,0)
	lg.setBlendMode("replace")
	light:draw()
	lg.setBlendMode("alpha")
	lg.setColor(0,0,0,255)
	lg.draw(light_img,playerX,playerY,0,2,2,light_img:getWidth()/2,light_img:getHeight()/2)
	
	lg.setCanvas()
	
	local x0,y0 = cam:worldCoords(0,0)
	lg.draw(light_box,x0,y0)
	lg.setColor(255,255,255)
end

function draw_all()
	
	lg.draw(mapData.bathes[1])

	if(DEBUG) then
		for _,v in pairs(mapData.points) do
			for _,k in pairs(v) do
				if(k.gid==0) then
					lg.circle("fill",k.x,k.y,30,10)
				else
					lg.draw(mapTs[k.gid].img,mapTs[k.gid].quad,k.x,k.y)
				end
			end
		end
	end
	drawBots()

	updLight()

end

function draw_ui()
	
	if(showJoy) then
		joy.draw()
	end
	
	if(DEBUG) then
	
		lg.setColor(0,0,255)
		lg.print("FPS:"..love.timer.getFPS(),10,12)
		lg.print(mem,10,24)
	
		local stats = love.graphics.getStats()
	    local str = string.format("Texture Memory: %.2f MB\n Draw Calls:%d", stats.texturememory / 1024 / 1024,stats.drawcalls)
	    lg.printf(str, 10, 36,300)

	    lg.setColor(255,255,255)
	end
	--for i=1,#BLogic.logs do lg.print(BLogic.logs[i],10,10+i*10) end
end

function love.draw()
	cam:draw(draw_all)
	draw_ui()
end

function love.keypressed(key)
	if(key=="backspace" or key=="escape")  then
			love.event.quit()
	end
end

function getNewShape(shapeData,group)
	local data = shapeData
	local shape={}
	
	if(shapeData==nil) then return nil  end
	
	if(data.shape=="rectangle") then
	
		shape=WC:addRectangle(data.x or 0,data.y or 0,data.width or 0,data.height or 0)
		
	elseif(data.shape=="polygon") then 
		local x,y = data.x,data.y
		local points={}
		
		for _,v in pairs(data.polygon) do
			table.insert(points,v.x+x)
			table.insert(points,v.y+y)
		end
		shape=WC:addPolygone(table.unpack(points))
		
	elseif(data.shape=="ellipse") then
		shape=WC:addCircle(data.x,data.y,(data.width+data.height)/2)
	end
	
	shape.name = data.name
	shape.properties = data.properties
	shape.type = data.type
	
	if(group~=nil and group~="") then
		WC:addToGroup(group,shape)
	end
	
	return shape
end

function vector.moveTo(from,to,speed)
		local x,y=from[1] or from.x,from[2] or from.y
		local x2,y2=to[1] or to.x,to[2] or to.y
		
		local dx,dy = x2-x,y2-y
		local len = math.sqrt(dx*dx+dy*dy)
		
		if(len/speed<1) then return {x=x2,y=y2} end
		
		return {x=x+dx*speed/len,y=y+dy*speed/len}
end 

function addBots(points)
		assert(points,"Point data=nil")
		local p=points["bot"]
		assert(p,"Point data [bot]=nil")
		
		local bimg = lg.newImage("sprites/bot.png")
		
		for _,v in pairs(p) do
				local b = WC:addCircle(v.x,v.y,18)
								b.type="bot"
								b.img=bimg
								b.s=0.5
								b.dir=vector(1,0)
								b.update=true
								b.range=100
								b.show=true
								b.timerun=0
								b.rspeed=2
								b.mspeed=120
								b.brain = state.newFSM(b)
								b.brain:push(BLogic.find)
				bots[#bots+1]=b
		end
	BLogic.setBots(bots)
end

function drawBots()
	lg.setColor(255,255,255)
	for _,v in pairs(bots) do
			if(v.show) then
				local iw,ih=0,0
				local x,y=v:center()
				
				local angle = v.dir:angleTo()
				
				if(v.anim) then
					iw,ih=v.anim.fh/2,v.anim.fw/2

					lg.setColor(0,0,0,180) --shadows
					v.anim:draw(x,y+8,angle or 0,v.s or 1,v.s or 1,iw,ih)
					lg.setColor(255,255,255,255)

					v.anim:draw(x,y,angle or 0,v.s or 1,v.s or 1,iw,ih)

				elseif(v.img) then

					iw,ih=v.img:getHeight()/2,v.img:getWidth()/2
					
					lg.setColor(0,0,0,180) --shadows
					lg.draw(v.img,x,y+8,angle or 0,v.s or 1, v.s or 1,iw,ih)
					lg.setColor(255,255,255,255)

					lg.draw(v.img,x,y,angle or 0,v.s or 1, v.s or 1,iw,ih)
					

				end
				if(DEBUG) then
					lg.setColor(0,0,200)
				 	v:draw() 
				 	lg.line(x,y,x+v.dir.x*(v.range or 20),y+v.dir.y*(v.range or 20))
				 	lg.setColor(255,255,255)
				end
			end
	end
end

function updateBots(dt)
	--state logic update
	for _,v in pairs(bots) do
		if(v.update) then	
			v.brain:update(dt)
		end
	end
end