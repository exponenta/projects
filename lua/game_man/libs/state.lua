local state={}
state.__index=state

function state.newFSM(obj)
	local m={}
		m.stack={}
		m.upd=true
		m.obj=obj
		return setmetatable(m,state)
end

function state:push(s)
	if(s~=self.stack[#self.stack]) then
		table.insert(self.stack,s)
	end
end

function state:pop()
	if(#self.stack>1) then
		self.stack[#self.stack]=nil
	end
end

function state:update(dt)
	if(#self.stack>0) then
		self.stack[#self.stack](dt,self.obj)
	end
end

function state:clean()
	local l=#self.stack
	for i=1,l do self.stack[i]=nil end
end

return state