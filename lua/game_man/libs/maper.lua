local lg=love.graphics
--local lm=love.mouse
local lf=love.filesystem

local SW,SH=lg.getWidth(),lg.getHeight()

local maper={}

function maper.loadMapFile(file)
	local file = file	
	local out = lf.load(file)
	assert(out,"File not load!")
	if(out) then return out() else return nil end 
end

function maper.getTileSet(map,mapfld)
	local ts={}
	local mapfld = mapfld
	
	if(map) then
		local lts = map.tilesets
		
		for i,v in pairs(lts) do
			local img = lg.newImage(mapfld..v.image)
			local tw,th = v.tilewidth,v.tileheight
			local marg, sp = v.margin or 0, v.spacing or 0
			local iw,ih = img:getWidth()-marg,img:getHeight()-marg
			
			local fin = v.firstgid
			local tcx,tcy = math.floor(iw/tw),math.floor(ih/th) 
			
			for y=0,tcy-1 do
				for x=0,tcx-1 do
					local frame = lg.newQuad(x*(tw+sp)+marg, y*(th+sp)+marg, tw, th, iw+marg, ih+marg)
					ts[fin+x+y*tcx]= {
						quad = frame,
						img = img,
						tw = tw,
						th = th
						}		
				end
			end
			
		end
		return ts
	end
	return nil
end

function maper.drawLayers(map,tiles,x,y)
	
	local lays = map.layers
	local tiles = tiles
	local mx,my = x or 0,y or 0
	
	for _,v in pairs(lays) do
			
		lg.setColor(255,255,255,math.floor(255* (v.opacity or 0)))
		
		if(v.type=="tilelayer") then
			local w,h = v.width,v.height
			local data = v.data
			
			for y=1,h do
				for x=1,w do
					local ind = data[x+(y-1)*w]
					local tile = tiles[ind]
					lg.draw(tile.img,tile.quad,mx+(x-1)*tile.tw,my+(y-1)*tile.th)
				end
			end
		end
			
	end
	
end

function maper.getNewShape(...)
	return nil
end

function maper.getLayersData(map,ts)	
	local lays = map.layers
	local tiles = ts
	--local mx,my = x or 0,y or 0
	local bathes={}
	local colliders={}
	local points={}
	local items={}
	
	for _,v in pairs(lays) do
			
		--lg.setColor(255,255,255,math.floor(255* (v.opacity or 0)))
		
		
		if(v.type=="tilelayer") then
		
			local w,h = v.width,v.height
			local data = v.data
			
			local batch = lg.newSpriteBatch(ts[1].img,SW*SH)
			--batch.opacity = v.opacity or 0
			batch:bind()
			batch:clear()
	
			for y=1,h do
				for x=1,w do
					local ind = data[x+(y-1)*w]
					local tile = tiles[ind]
					
					if(ind>0) then 
						batch:add(tile.quad,(x-1)*tile.tw,(y-1)*tile.th)
					end
					
				end
			end
			
			batch:unbind()
			table.insert(bathes,batch)
			
		elseif (v.type=="objectgroup") then
			if(v.name=="colliders") then
				local objects=v.objects
				for _,s in pairs(objects) do
					local col = maper.getNewShape(s,"walls")
					table.insert(colliders,col)
				end
			end
			if(v.name=="points" or v.name=="items") then
				local objects=v.objects
				for _,s in pairs(objects) do
					local p = {x=s.x,y=s.y,type=s.type,gid=s.gid or 0,properties=s.properties}
					if(v.name~="items") then
						if(points[s.name]==nil) then points[s.name]={} end
						table.insert(points[s.name],p)
					else
						table.insert(items[s.name],p)
					end
						
				end
			end
		end
		
	end
	
	return {bathes=bathes,colliders=colliders,points=points,items=items}
end

return maper
