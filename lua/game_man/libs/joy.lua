
local joy={}
local lg=love.graphics
local SW,SH=lg.getWidth(),lg.getHeight()

function joy.init(x,y,d)
	joy.px=x or 100
	joy.py=y or lg.getHeight()-100
	joy.d1=0
	joy.d2=d or 80
	joy.x=0
	joy.y=0
	joy.fi=0
	joy.a=false
end

function joy.update()
	local jx,jy = joy.px or 50, joy.py or SH-50
	local d1,d2 = joy.d1 or 10,joy.d2 or 40

	if(love.touch~=nil) then
		if(love.touch.getTouchCount()>0) then
		
			local i,lx,ly = love.touch.getTouch(1)
			lx,ly=lx*SW,ly*SH
			
			local len = (lx-jx)*(lx-jx)+(ly-jy)*(ly-jy)
			len = math.sqrt(len)
			
			if(len<d2) then 
				joy.x = (lx-jx)/d2
				joy.y = (ly-jy)/d2
				joy.a=true
				joy.fi=i
			elseif(joy.a==true and joy.fi==i) then
				joy.x = (lx-jx)/len
				joy.y = (ly-jy)/len			
			else
				joy.a=false
				joy.x=0
				joy.y=0	
			end
			
		else
			joy.a=false
			joy.x=0
			joy.y=0
		end
	end
	
end

function joy.draw()
	lg.setColor(255,100,100)
	lg.circle("line",joy.px,joy.py,joy.d2,8)
	lg.setColor(0,0,200)
	lg.circle("fill",joy.px+joy.x*joy.d2,joy.py+joy.y*joy.d2,joy.d2/4,8)
	lg.setColor(255,255,255)
end

return joy