local lg=love.graphics
local state = require "libs.state"
local vector = require "libs.vector"

local SW,SH = lg.getWidth(),lg.getHeight()
local bots={}
local logs={}

function move(dt,bot)
	bot:move(bot.dir.x*bot.mspeed*dt,bot.dir.y*bot.mspeed*dt)
	bot.brain:pop()
	table.insert(logs,1,"move")
end

function setBots(bts)
	bots=bts
end

function bound(dt,bot)
	if(bot.x>SW) or bot.x<0 then 
		bot.dir.x=-bot.dir.x
		if(bot.x> SW) then bot.x=SW-10 else bot.x=10 end
	end
	
	if(bot.y>SH) or bot.y<0 then 
		bot.dir.y=-bot.dir.y
		if(bot.y > SH) then bot.y=SH-10 else bot.y=10 end 
	end

	bot.brain:pop()
end

function find(dt,bot)
	
	table.insert(logs,1,"find")

	if(not bot.prop) then bot.prop={} end
	bot.prop.target=nil

	local bot_pos = vector(bot:center())
	--assert(false,#bots)
	for _,b in pairs(bots) do
		if(b~=bot and b.type=="player") then
			local b_pos = vector(b:center())

			local len = vector.dist(b_pos,bot_pos)
			
			if(len<120) then 
				bot.prop.target=b
				bot.brain:push(follow)
			end

		end
	end
	bot.brain:push(move)
end

function follow(dt,bot)
	table.insert(logs,1,"follow")

	local  target = bot.prop.target
	if target==nil then bot.brain:pop() end
	
	local bot_pos = vector(bot:center())
	local target_pos = vector(target:center())

	local len = vector.dist(bot_pos,target_pos)

	bot.dir.x = -(bot_pos.x-target_pos.x)/len
	bot.dir.y = -(bot_pos.y-target_pos.y)/len
	
	if(len<100) then
		if(len>80) then
			local tmp = bot.dir.x
			bot.dir.x=bot.dir.y
			bot.dir.y=-tmp
		else
			bot.dir.x=-bot.dir.x
			bot.dir.y=-bot.dir.y
		end
	end

	bot.brain:push(move)
end

return {find=find,move=move,follow=follow,bound=bound,setBots=setBots, logs=logs}