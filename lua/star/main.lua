local lg = love.graphics;
local lf = love.filesystem;


local starMaps={}

function readStarMap(fname)

	local starMaps = {}
	file=io.input(fname)
	for i=1,2 do
		local _c = file:read("*n");
		starMaps[i]={}
		local ss = {}
		for j=1,_c do
			local a,b = file:read("*n"),file:read("*n")
			local s = {}
			table.insert(s,a)
			table.insert(s,b)
			table.insert(ss,s)
		end
		starMaps[i]=ss;
	end
	file:close()
	return starMaps;
end

local diff={}

local sx,sy=0,0
local rx,ry=0,0

function love.load( ... )
 	starMaps=readStarMap("stars.txt")
 	--center
 	for i,map in ipairs(starMaps) do

 		local c={x=0,y=0}			
 		
 		for j,data in ipairs(map) do
 			c.x=data[1]+c.x
 			c.y=data[2]+c.y
 		end
 		c.x=c.x/#map
 		c.y=c.y/#map
 		diff[i]={center=c};
 	end

 	rx,ry=diff[2].center.x,diff[2].center.y

end


local angle = 0
function love.keypressed(k,i)
	if(k=="up")then
		sy=sy-0.5
	elseif(k=="down") then
		sy=sy+0.5
	end 

	if(k=="left")then
		sx=sx-0.5
	elseif(k=="right") then
		sx=sx+0.5
	end 

	if(k=="z")then
		angle=angle-0.05
	elseif(k=="x") then
		angle=angle+0.05
	end 
	
end

local mx,my=0,0

function love.update( ... )
	mx,my = love.mouse.getPosition()
end

local fx,fy,find=0,0,false

function love.mousepressed( x, y, b )
		fx,fy=x,y
		find=true
end
local id = {0,0}
function love.draw( ... )
	for i,map in ipairs(starMaps) do

		lg.setColor((i-1)*255,255,0)
		local dx = diff[i].center.x
		local dy = diff[i].center.y

		for j,v in ipairs(map) do
			local dpx,dpy=(v[1])*5+lg.getWidth()/2,(-v[2])*5+lg.getHeight()/2
			if(i==2) then 
				dpx=dpx+sx
				dpy=dpy+sy
			end

			if(math.abs(dpx-mx)<5 and math.abs(dpy-my)<5) then
				if(find==true) then
					rx = dpx
					ry = dpy
					find=false
				end
					id[i] = j;
			end
				

				--local len = math.sqrt((dpx-rx)*(dpx-rx)+(dpy-ry)*(dpy-ry))
				--dpx=dpx+math.cos(angle)*len
				--dpy=dpy+math.sin(angle)*len
						
			lg.circle("fill",dpx,dpy,4,10);

		end
	end
	lg.print("DR: "..sx..":"..sy,10,10)
	lg.print("RR: "..rx..":"..ry..":angle:"..angle,10,30)
	lg.print("ID:"..(id[1]-1)..":"..(id[2]-1),10,50)
	
end