local socket = require "socket"
local camera = require "camera"

require "tserial"

--get curent IP

local serverIP = ""

local lg = love.graphics

local udp
local screens = {} -- the empty screens-state
local balls = {}

local data, msg_or_ip, port_or_nil
local entity, cmd, parms

local mSizeAuto = true
local mapW,mapH,mapX,mapY = 0,0,0,0
local mapMaxH = 1700

local mapResize = 0.25

local cam = camera(lg.getWidth()/2*4,lg.getHeight()/2*4,mapResize,0)

function love.load()
    local s = socket.udp()
    s:setpeername("74.125.115.104",80)
    local ip, _ = s:getsockname()
    s:close()

    udp = socket.udp()
    udp:settimeout(0)
    udp:setsockname(ip, 1234)
    font1= lg.newFont(math.floor(12*1/mapResize))
    font2= lg.newFont(12)
    lg.setFont(font1)
end


local function sortBox()
    local w = screens
    local minX,minY,maxW,maxH = 10000,10000,0,0

    for i,v in pairs(w) do
        --if(last) then
            if(mSizeAuto and not v.disconnect) then
                if(minX>v.x) then minX=v.x end
                if(minY>v.y) then minY=v.y end

                if(maxW<v.x+v.sw*v.scale) then maxW=v.x+v.sw*v.scale end
                if(maxH<v.y+v.sh*v.scale) then maxH=v.y+v.sh*v.scale end

            end 
    end

   mapX,mapY=minX,minY
   mapW,mapH=maxW-minX,maxH-minY
                
end

local  screensCount = 0
local function addScreen(s)
    local s=s
    if(not s.scale) then s.scale=1 end
    
    s.name=("screen_"..tostring(screensCount))

    if(s) then
        for i,v in pairs(screens) do
          
          if(s.ip==v.ip and v.disconnect) then 
            v.disconnect=false
          else
              if(RectsOverlap(s.x,s.y,s.sw*s.scale,s.sh*s.scale,v.x,v.y,v.sw*v.scale,v.sh*v.scale)) then
               
                if(v.y+v.sh*v.scale+s.sh>mapY+mapMaxH) then 
                    s.x=v.x+v.sw*v.scale
                    s.y=mapY
                else
                    --s.x=mapX
                    s.y=v.y+v.sh*v.scale
                end
              end
          end

        end
        screensCount=screensCount+1
        if(not s.name) then return false end
        screens[s.name]=s

    end

    return s
end

function RectsOverlap(x1, y1, w1, h1, x2, y2, w2, h2)
    if x1 > (x2 + w2) or (x1 + w1) < x2 then return false end
    if y1 > (y2 + h2) or (y1 + h1) < y2 then return false end
    return true
end

local clientFindTime = 1
local time = 0

function love.update(dt)
    updateBalls(dt)
    transformedScreen()

    time=time+dt
    
    if(time>clientFindTime) then
        updScreens()
        time=0
    end

    data, msg_or_ip, port_or_nil = udp:receivefrom()
    if data then

        local table = Tserial.unpack(data,true)
        if(table.msg=="at") then
            --screens[table.name]={x=0,y=0,name=table.name,sw=table.sw,sh=table.sh,scale=1}
            table.ip = msg_or_ip
            local s = addScreen(table)
            if(s) then
                newItem = {msg="succes",newName=s.name}
                sortBox()
            else
                newItem = {msg="error"}
            end

            local ret = Tserial.pack(newItem)
            udp:sendto(ret,msg_or_ip,port_or_nil)
        end

        if(table.msg=="addball") then
            local p = screens[table.parent]
            if(p) then
                local b = {x=table.x*p.scale+p.x,y=table.y*p.scale+p.y,r=table.r*p.scale,parent=table.parent}
                balls[table.name] = b
            end 
        end
        
        if(table.msg=="update" and screens[table.name]) then
            local bnew = {}
            local p = screens[table.name]
            p.beUpdated=true

            for k,v in pairs(balls) do
                bnew[k]={}
                bnew[k].parent=v.parent
                bnew[k].x = (v.x-p.x)/p.scale
                bnew[k].y = (v.y-p.y)/p.scale
                bnew[k].r = v.r/p.scale
                bnew[k].sx = v.sx
                bnew[k].sy = v.sy

            end
            local ret=Tserial.pack({msg="update",balls=bnew})
            udp:sendto(ret,msg_or_ip,port_or_nil)
        end

    end       
    socket.sleep(0.01)
end

function love.draw()
    lg.setFont(font1)
    cam:draw(draw)
    lg.setFont(font2)
    local ip, port = udp:getsockname() 
    lg.print("Server IP:"..(ip or "error")..":"..port,10,10)
    lg.print(mapW..":"..mapH,10,40)
end


local colors = {focus={0,0,200},click={0,200,200},normal={255,255,255}}

function draw() 
    
    lg.setColor(255,255,255)
    for _,b in pairs(balls) do
        lg.circle("fill",b.x,b.y,b.r,b.r/8)
    end

    for _, v in pairs(screens) do
    
        if(v.focus) then lg.setColor(colors.focus) 
        else
            lg.setColor(colors.normal)
        end

        if(v.click) then lg.setColor(colors.click) end

        lg.rectangle("line",v.x,v.y,v.sw*v.scale,v.sh*v.scale)
        
        lg.print(v.name,v.x+10,v.y+10)
        lg.print(v.ip,v.x+10,v.y+42)
        lg.print(v.beUpdated and "Yes" or "Not",v.x+10,v.y+84)
    end

     --if(mapW>0) then
        lg.setColor(200,0,0,90)
        lg.rectangle("fill",mapX,mapY,mapW,mapH)
    --end
end

function moveBalls()
    for k,b in pairs(balls) do
        b.x=b.x+mapX
        b.y=b.y+mapY
    end
end

function updateBalls(dt)
    for k,b in pairs(balls) do
        if(not b.sx) then b.sx=love.math.random(-1,1)*300 end
        if(not b.sy) then b.sy=love.math.random(-1,1)*300 end

        b.x=b.x+b.sx*dt
        b.y=b.y+b.sy*dt

        if(b.y-b.r<mapY) then b.sy=-b.sy end
        if(b.x-b.r<mapX) then b.sx=-b.sx end
        if(b.y+b.r>mapH+mapY) then b.sy=-b.sy end
        if(b.x+b.r>mapW+mapX) then b.sx=-b.sx end
    end
   
end

function updScreens()
    local beD = false
    for k,s in pairs(screens) do
        if(s.beUpdated) then
            s.beUpdated=false
        else
            s.disconnect=true
            beD=true
            screensCount=screensCount-1
        end
    end
    if(beD) then sortBox() end
end

local olx,oly = 0,0
function transformedScreen()
    local mx,my = love.mouse.getPosition()
    mx,my = cam:worldCoords(mx,my)
    local needUpdate = false
    
    for k,s in pairs(screens) do
        if(RectsOverlap(s.x,s.y,s.sw*s.scale,s.sh*s.scale,mx,my,1,1)) then
            
            s.focus=true
            s.click=love.mouse.isDown("l")

            if(s.click) then
                s.x=s.x+mx-olx
                s.y=s.y+my-oly
                needUpdate=true
            end

        else
            s.focus=false
        end
    end
    olx,oly=mx,my
    if(needUpdate) then sortBox() end
end
