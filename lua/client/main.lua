local socket = require "socket"

require "tserial"
local lg = love.graphics
local SW,SH = lg.getWidth(),lg.getHeight()

local address, port = "192.168.0.22", 1234


local updaterate = 0.05 -- how long to wait, in seconds, before requesting an update

local balls = {} -- the empty world-state
local newBalls = {} --recieved data

local objectData = {x=0,y=0,msg="at",name="lol",radius=40,sw=SW,sh=SH}

local beConnected = false

function love.mousepressed(x,y,k)
        local ball = {msg="addball",x=x,y=y,r=100,parent=objectData.name,name="ball_"..objectData.name}
        local  str = Tserial.pack(ball)
        udp:send(str)
end

function love.load()

    udp = socket.udp()
    udp:settimeout(0)
    udp:setpeername(address, port)

    math.randomseed(os.time())
   

    objectData.name=tostring(math.random(99999))

end

-- love.update, hopefully you are familiar with it from the callbacks tutorial
local updInters = 0.1
local beSendUpdate=false
local time = 0

function love.update(dt)   
    updateBalls(dt)

    time=time+dt

    if(time>updInters) then
        if(not beConnected) then 
             objectData.msg="at"
             local ret = Tserial.pack(objectData)
             udp:send(ret)
        else
            local ret=Tserial.pack({msg="update",name=objectData.name})
            udp:send(ret)
            
        end
        time=0
    end

    
    repeat 
        data,msg = udp:receive()
        if(data) then
            
            local table = Tserial.unpack(data,true)

            if(table.msg=="at") then
                world[objectData.name]={x=table.x,y=table.y,name=objectData.name,radius=table.radius}
            end
            
            if(table.msg=="error") then 
                    objectData.msg="at"
                    res = Tserial.pack(objectData)
                    udp:send(res)
            end
            if(table.msg=="succes") then
                beConnected=true
                objectData.name=table.newName
            end
            
            if(table.msg=="addball") then
                balls[table.name]=table
            end
            
            if(table.msg=="update")then
                balls=table.balls or {}
            end

        end
        
    until not data

end

-- love.draw, hopefully you are familiar with it from the callbacks tutorial
function love.draw()
    -- pretty simple, we just loop over the world table, and print the
    -- name (key) of everything in their, at its own stored co-ords.
    for _, v in pairs(balls) do
        lg.setColor(v.color or {255,255,255})
        lg.circle("fill",v.x,v.y,v.r,v.r)
    end

    lg.print(objectData.name,10,10)
    lg.print("Err:"..msg,10,20)


end

function updateBalls(dt)
    local bs = balls

    for k,b in pairs(bs) do
        b.x=b.x+b.sx*dt
        b.y=b.y+b.sy*dt
    end
end
