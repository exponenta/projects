local lg=love.graphics
local light={}
light.__index = light
local shape_ = {}

function setBoundsShape(s)
	shape_=s
end

function addLight(r,cp)
	local cp = cp+2 or 38
	local r = r or 100
	local l={}

	l.mesh = lg.newMesh(cp)
	l.x = 0
	l.y = 0
	l.r=r
	l.cp=cp

	for i=2,cp do
		local x  = r*math.cos(2*i*math.pi/(cp-2))
		local y  = r*math.sin(2*i*math.pi/(cp-2))
		l.mesh:setVertex(i,x,y,0,0)
	end

	return setmetatable(l,light)
end

function light:update()
	local sh = shape_
--	local beUpdated=false
		--if(vector.dist(sx,sy,self.x,self.y) < sr+self.r) then
	for i=2,self.cp do

			local dx,dy = self.mesh:getVertex(i)
			local len = math.sqrt(dx*dx+dy*dy)		
			--assert(false,dx..":"..dy)
			local _t=math.huge

			for _,s in pairs(sh) do

				local inters,t = s:intersectsRay(self.x,self.y,dx/len,dy/len)
				
				if(inters and t<self.r and t>0) then
					_t=math.min(t,_t)
				end
			end

			local _x,_y = self.r*dx/len,self.r*dy/len
			if(_t~=math.huge) then
				_x = _t*dx/len
				_y = _t*dy/len

			end
			self.mesh:setVertex(i,_x,_y,0,0)
	end
	--if(beUpdated) then
		--self.tris = love.math.triangulate(self.poly)
	--end
end

function light:move(x,y)
	if(x==nil and y==nil) then return end 
	local x,y = x,y
	if(y==nil) then x,y = unpack(x) end
	self.x,self.y = x,y
	self:update()
end

function light:draw()
	--[[for i=0,#self.poly/2-1 do
			local x1 = self.poly[i*2+1]+self.x
			local y1 = self.poly[i*2+2]+self.y

			local x2i = (i*2+3)
			local y2i = (i*2+4)

			if(x2i>#self.poly-1) then x2i=1 end
			if(y2i>#self.poly) then y2i=2 end

			local x2 = self.poly[x2i]+self.x
			local y2 = self.poly[y2i]+self.y	

			lg.polygon("fill",{x1,y1,x2,y2,self.x,self.y})
	end]]--
	lg.draw(self.mesh,self.x,self.y)
end
return {addLight = addLight,setBoundsShape = setBoundsShape}