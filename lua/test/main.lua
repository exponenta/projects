local lg=love.graphics
local lm=love.mouse
local lf=love.filesystem
local SW,SH=lg.getWidth(),lg.getHeight()
local HC = require("hardoncollider")()
local sLight = require("slight")

function love.load()
	shapes={
		HC:addRectangle(200,200,200,50),
		HC:addRectangle(200,200,20,200)
	}

	l = sLight.addLight(100,360)
	sLight.setBoundsShape(shapes)
end

local t = 0
local mx,my=0,0

function love.update(dt)
	mx,my = lm.getPosition()
	l:move(mx,my)
end

function addLight(r,cp)
	local cp = cp or 36
	local r = r or 100
	local l={}

	l.poly = {}
	l.x = 0
	l.y = 0
	l.r=r
	l.s=cp

	for i=0,cp-1 do
		l.poly[#l.poly+1] = r*math.cos(2*i*math.pi/cp)
		l.poly[#l.poly+1] = r*math.sin(2*i*math.pi/cp)
	end

	return setmetatable(l,light)
end


function love.draw()
	for _,s in pairs(shapes) do
		s:draw("fill")
	end
	lg.setColor(255,0,0)
	l:draw()
	lg.setColor(255,255,255)
	 
end