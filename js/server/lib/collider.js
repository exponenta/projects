//var Vec3 = require('./math_vec3');

reflect = function (a, n) {
	var _n = n;
	var _tmp = [_n[0], _n[1]];
	var _v = a;

	var d = 2 * (_n[0] * _v[0] + _n[1] * _v[1]);

	_tmp[0] *= d;
	_tmp[1] *= d;

	_v[0] -= _tmp[0];
	_v[1] -= _tmp[1];

	return _v;
};

// from http://habrahabr.ru/post/148325/
var collider = [];


collider.reflect = reflect;

collider.callback = function (first, second, n) {
	console.log("[math] " + first.name + " touch " + second.name);
	//console.log("pos: ["+first.position+"],vel:["+first.velocity+"]");

};

collider.testLine2 = function (entity, line) {

	var epos = entity.position || entity.def_pos;
	var lpos = line.position || line.def_pos;
	var size = line.size;

	var vel = entity.velocity;

	var n = collider.testLine(epos, [lpos[0] + size[0], lpos[1] + size[1]], [lpos[0] - size[0], lpos[1] - size[1]]);

	if (n !== null) {

		var n_l = Math.sqrt(n[0] * n[0] + n[1] * n[1]);

		var dr = n_l - entity.radius;

		var dot_v_n = vel[0] * n[0] + vel[1] * n[1];

		//console.log("dot_v_n:"+dot_v_n);

		if (dr <= 0 && dot_v_n <= 0) {

			collider.callback(entity, line, n);

			n[0] /= n_l;
			n[1] /= n_l;

			if (!line.trigger) {

				entity.position[0] += n[0] * dr;
				entity.position[1] += n[1] * dr;

				entity.velocity = reflect(vel, n);
			}

			return n;
		}
	}

	return null;
};

collider.testLine = function (a, start, end) {

	var line = [end[0] - start[0], end[1] - start[1]];
	var p = [end[0] - a[0], end[1] - a[1]];
	var ll = line[0] * line[0] + line[1] * line[1]; //line len
	ll = (p[0] * line[1] - p[1] * line[0]) / ll; //  нормальзованое косое произведение, или cross

	var d1 = p[0] * line[0] + p[1] * line[1];
	var m1 = [start[0] - a[0], start[1] - a[1]];
	var d2 = -(m1[0] * line[0] + m1[1] * line[1]);

	if (d1 > 0 && d2 > 0) {
		return [-line[1] * ll, line[0] * ll];
	}
	return null;
};

collider.testBox = function (entity, box) {

	var bpos = box.position || box.def_pos;
	var size = box.size;
	var epos = entity.position || entity.def_pos;

	var line = {
		position: [bpos[0], bpos[1] + size[1], 0],
		size: [size[0], 0, 0],
		name: box.name,
		trigger: box.trigger
	};
	var corner = {
		position: [bpos[0] + size[0], bpos[1] + size[1], 0],
		radius: 0.01,
		name: box.name,
		trigger: box.trigger
	};

	var n = collider.testLine2(entity, line) || collider.testCurcles(entity, corner);
	if (n)
		return n;

	line.position = [bpos[0] + size[0], bpos[1], 0];
	line.size = [0, size[1], 0];

	corner.position = [bpos[0] + size[0], bpos[1] - size[1]];

	var n = collider.testLine2(entity, line) || collider.testCurcles(entity, corner);
	if (n)
		return n;

	line.position = [bpos[0], bpos[1] - size[1], 0];
	line.size = [size[0], 0, 0];


	corner.position = [bpos[0] - size[0], bpos[1] - size[1]];

	var n = collider.testLine2(entity, line) || collider.testCurcles(entity, corner);
	if (n)
		return n;

	line.position = [bpos[0] - size[0], bpos[1], 0];
	line.size = [0, size[1], 0];


	corner.position = [bpos[0] - size[0], bpos[1] - size[1]];

	return collider.testLine2(entity, line) || collider.testCurcles(entity, corner);
};

collider.testCurcles = function (entity, circle) {

	var epos = entity.position || entity.def_pos;
	var bpos = circle.position || circle.def_pos;

	var vel = entity.velocity || [0, 0];

	var n = [epos[0] - bpos[0], epos[1] - bpos[1]];

	var n_l = Math.sqrt(n[0] * n[0] + n[1] * n[1]);

	var dr = n_l - (entity.radius + circle.radius);

	var dot_v_n = vel[0] * n[0] + vel[1] * n[1];

	if (dr <= 0 && dot_v_n < 0) {
		collider.callback(entity, circle, n);

		n[0] /= n_l;
		n[1] /= n_l;

		if (!circle.trigger) {

			entity.position[0] += n[0] * dr * 1.2;
			entity.position[1] += n[1] * dr * 1.2;

			entity.velocity = reflect(vel, n);

		}

		return n;
	}

	return null;
};
//--------------------------

collider.test = function (src, dsr) {


	if (dsr.type == "circle") {

		return collider.testCurcles(src, dsr);

	} else if (dsr.type == "box") {

		return collider.testBox(src, dsr);

	} else if (dsr.type == "line") {

		return collider.testLine2(src, dsr);
	}
};

module.exports = collider;