pc.script.attribute("loopCount","number",30);

pc.script.create('loop', function (app) {
    // Creates a new Loop instance
    var Loop = function (entity) {
        this.entity = entity;
    };

    Loop.prototype = {
        // Called once after all resources are loaded and before the first update
        initialize: function () {
            this.dynamics = [];
            this.statics = [];
            
            var pool = app.root.findByName("pool").getChildren();
            
            for(var l=0;l<pool.length;l++)
                {
                    var e = pool[l];
                    if(e.script && e.script.controller.isDynamic){
                        this.dynamics.push(e);
                    }else{
                        this.statics.push(e);
                    }
                }
                
            console.log("Dynamics:"+this.dynamics.length);
            console.log("Statics:"+this.statics.length);
            
            this.lp = app.root.findByName("lp");
        },
        
        //дааааа!!! я тебя сделал
        testLine: function(a,start,end){
            
            var line = end.clone().sub(start); 
            var p = end.clone().sub(a);
            var d = ( p.x * line.y - p.y * line.x ) / line.length();
           // console.log("d:"+d);
            var n = new pc.Vec3(-line.y,line.x,0).normalize();

            var d1 = p.dot(line);
            var m1 = start.clone().sub(a);
            var d2 = m1.dot(line.clone().scale(-1));
            
            if(d2>0 && d1>0){
                return n.scale(d).clone();
            }
            return null;
        },
        
        testBox: function(entity,box){
            
            var epos = entity.getPosition();
            var bpos = box.getPosition();
            epos.sub(bpos);
            
            var n;
            //console.log("[box] size:"+box.size.toString());
            
            var start  = new pc.Vec3(-box.size.x,box.size.y,0);
            var end  = new pc.Vec3(box.size.x,box.size.y,0);
            
            n = this.testLine(epos,start,end);
            //console.log("[box test 1] normal:"+n?n.toString():"null");
            
            start  = new pc.Vec3(box.size.x,box.size.y,0);
            end  = new pc.Vec3(box.size.x,-box.size.y,0);
            
            n = this.minVec( this.testLine( epos,start,end) , n);
            
            
            start  = new pc.Vec3(box.size.x,-box.size.y,0);
            end  = new pc.Vec3(-box.size.x,-box.size.y,0);
            
            n = this.minVec( this.testLine(epos,start,end) , n);
            
            start  = new pc.Vec3(-box.size.x,-box.size.y,0);
            end  = new pc.Vec3(-box.size.x,box.size.y,0);
            
            n = this.minVec( this.testLine(epos,start,end), n);
        
        
            if(n){
                var dr = n.length()-entity.radius;
            
                if(entity.velocity && dr<=0 && entity.velocity.dot(n)<0 )
                    {
                        n.normalize();
                        entity.translate(n.scale(dr));
                        n.normalize();
                        
                        entity.velocity = this.reflect(n,entity.velocity).normalize();
                        return n;
                    }
            }
            return null;
                
        },
        
        testCurcles: function(entity,curcle){
            
            var epos = entity.getPosition();
            var bpos = curcle.getPosition();
            
            var n = epos.clone().sub(bpos);
             //n.no
            if(n){
                 var dr = n.length()-entity.radius-curcle.radius;
            
                if(entity.velocity && dr<=0 && entity.velocity.dot(n)<0)
                    {
                        n.normalize();
                        entity.translate(n.scale(dr));
                     
                        entity.velocity = this.reflect(n.normalize(),entity.velocity).normalize();
                        return n;
                    }
            }
            return null;
        },
        //Нужна проверка на то, куда летит объект и менять в следствии этого скорость, что бы не было дрыганей
        update: function (dt) {
         
            for(var _n=0;_n<this.loopCount;_n++){
                
                for(var i=0;i<this.dynamics.length;i++){
                    var d_ent = this.dynamics[i];
                    
                    for(var j=0;j<this.statics.length;j++){
                        var s_ent = this.statics[j];
                        
                        if(s_ent.shape!=="box"){
                            var n = this.testCurcles(d_ent,s_ent);
                        }else{
                            var n = this.testBox(d_ent,s_ent);
                        }
                    }
                    
                    d_ent.translate(d_ent.velocity.clone().scale(d_ent.speed*dt / this.loopCount));
                   // console.log(d_ent.speed*dt);
                }
            }
        }
    };

    return Loop;
});
