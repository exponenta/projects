#!/bin/sh
killall -9 node
WORK_PATH=/web/pong.musicboxmaniacs.com
SRC_PATH=${WORK_PATH}/source
cd ${SRC_PATH}
git checkout --
git pull origin master
nohup supervisor index.js &
