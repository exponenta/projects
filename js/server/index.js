var WebSocketServer = require('./lib/Server');
//var sleep = require('sleep');
var uuid = require('node-uuid');
var collider = require('./lib/collider');
var dateFormat = require('dateformat');

var look = require('look');

if (look != null) {
    look.start(3131);
}

var arrayGetBy = function (arr, s, t, lim) {
    t = t || "id";
    lim = lim || arr.length;
    lim = Math.min(lim, arr.length);
    for (i = 0; i < lim; i++) {
        if (arr[i].hasOwnProperty(t) && arr[i][t] == s) {
            return arr[i];
        }
    }
    return null;
};

var SERVER_META = {
    PORT: 8080,
    VERSION: 5,
    log: true
};

var console_log = console.log;
if (!SERVER_META.log) {
    console.log = function () {
    };
} else {
    console.log = function () {
        var date = new Date();
        console_log("[" + dateFormat(date, "HH:MM:ss") + "]", arguments);
    };
}

var shortId = function (uuid) {
    return uuid.split('-')[1];
};

console.log('Starting the server', SERVER_META);

var server = new WebSocketServer({
    port: SERVER_META.PORT
});

var worldData = require('./map.json');
for (var i = 0; i < 4; i++) {
    worldData.panels[i].id = uuid.v4();
    worldData.panels[i].fixed = false;
    worldData.statics[i].panelID = worldData.panels[i].id;
    worldData.panels[i].position = [worldData.panels[i].def_pos[0], worldData.panels[i].def_pos[1]];
}

var game = [];

game.newBot = function (panelID) {
    var names = ["[bot] iSHOK", "[bot] BUGAENKO", "[bot] NIK-NIK", "[bot] A.N.PLUSONE"];
    var w = [1, 0.7, 0.5, 0.3];
    var l = 0;

    while (arrayGetBy(gameData.players, names[l], "name") != null && l < names.length) {
        l++;
    }

    return {
        id: "bot",
        name: names[l],
        brain: w[l],
        score: 0,
        panelID: panelID
    };
};

var gameData = {
    timeStep: 1000 / 60,
    mathStep: 20,
    maxBalls: 4,
    newBallSpawnTime: 8000,
    ballSpeed: 12,
    panelSpeed: 16,
    countOfBots: 4,
    activeClients: {},
    players: []
};

gameData.players.push(game.newBot(worldData.panels[0].id));
gameData.players.push(game.newBot(worldData.panels[1].id));
gameData.players.push(game.newBot(worldData.panels[2].id));
gameData.players.push(game.newBot(worldData.panels[3].id));

server.sendAll = function (type, data) {
    for (var id in gameData.activeClients) {
        if (gameData.activeClients.hasOwnProperty(id)) {
            try {
                gameData.activeClients[id].send(type, data);
            } catch (e) {
                console.log(e);
                server.userDisconnect(gameData.activeClients[id], true);
            }
        }
    }
};

server.userDisconnect = function (client, lost) {
    delete gameData.activeClients[client.id];
    //removing disconnected client from players
    var played = false;
    for (var i = 0; i < gameData.players.length; i++) {
        if (gameData.players[i].id === client.id) {
            //detach panel from player
            played = i < 5;
            console.log("player lost:", gameData.players[i].name);
            if (gameData.players[i].panelID != null) {
                game.teamEnd(gameData.players[i].panelID, true);
            } else {
                //del without teamEnd
                if (gameData.players.length == 4) {
                    gameData.players.splice(i, 1);
                    gameData.players.push(game.newBot(null));
                } else {
                    gameData.players.splice(i, 1);
                }
            }
            break;
        }
    }

    server.sendAll('userDisconnected', {
        id: client.id,
        name: client.data.name,
        total: Object.keys(gameData.activeClients).length,
        lost: lost ? true : false,
        played: played
    });
};

server.on('connection', function (client) {
    var id = shortId(client.id);

    client.on('ping', function (data) {
        console.log('ping', id, data);

        if (data.version !== SERVER_META.VERSION) {
            client.send('requestError', {
                message: 'Wrong client version, please update to the latest one'
            });
            return;
        }

        client.data = {};
        gameData.activeClients[client.id] = client;
        console.log('userConnected', id);
        console.log('total clients', Object.keys(gameData.activeClients).length);

        client.send('pong', {
            id: client.id,
            players: gameData.players,
            panels: worldData.panels
        });

        server.sendAll('userConnected', {
            id: client.id,
            total: Object.keys(gameData.activeClients).length
        });
    });

    client.on('enter', function (data) {
        console.log(id, 'enter', data);

        gameData.activeClients[client.id].data.name = data.name;

        var newPlayer = {
            id: client.id,
            name: data.name,
            score: 0,
            panelID: null
        };

        if (gameData.players.length < 4) {
            for (var i = 0; i < worldData.panels.length; i++) {
                var player = arrayGetBy(gameData.players, worldData.panels[i].id, "panelID", 4);
                if (player == null) {
                    newPlayer.panelID = worldData.panels[i].id;
                    break;
                }
            }
        }

        gameData.players.push(newPlayer);

        client.send('welcome', {
            message: 'welcome ' + data.name
        });

        server.sendAll('userEntered', {
            name: data.name,
            id: client.id
        });

    });

    client.on('disconnect', function () {
        server.userDisconnect(client)
    });

    client.on("inputEnter", function (data) {
        for (var i = 0; i < Math.min(gameData.players.length, 4); i++) {
            if (gameData.players[i].id == client.id) {
                for (var j = 0; j < 4; j++) {
                    if (worldData.panels[j].id == gameData.players[i].panelID) {

                        worldData.panels[j].moveTo = null;
                        worldData.panels[j].dir = null;

                        var direction = worldData.panels[j].direction;

                        if (data.pos != null) {
                            var pos = worldData.panels[j].position || worldData.panels[j].def_pos;
                            var dir = [data.pos[0] - pos[0], data.pos[1] - pos[1]];
                            var l = Math.sqrt(dir[0] * dir[0] + dir[1] * dir[1]);
                            if (l !== 0) {
                                dir[0] /= l;
                                dir[1] /= l;
                            }
                            dir[0] *= direction[0];
                            dir[1] *= direction[1];

                            worldData.panels[j].dir = dir;

                            worldData.panels[j].moveTo = [data.pos[0] * direction[0], data.pos[1] * direction[1]];
                        } else if (data.dpos != null) {
                            worldData.panels[j].dir = [data.dpos[0] * direction[0], data.dpos[1] * direction[1]];
                        }
                        break;
                    }
                }
                break;
            }
        }
    });
});


process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err.stack);
});

///looop --------------------


// preparing panels


collider.callback = function (f, s, n) {
    if (s.tag && s.tag == "wall") {
        game.teamEnd(s.panelID);
    }
};

game.killBot = function (panelID) {
    for (i = 0; i < gameData.bots.length; i++) {
        if (gameData.bots[i].panelID == panelID) {
            console.log("Killed bot:", gameData.bots[i].name);
            gameData.bots.splice(i, 1);

            break;
        }
    }
};


game.botsUpdate = function (dt, ball) {
    if (worldData.dynamics.length == 0) {
        return;
    }

    for (var i = 0; i < 4; i++) {

        var bot = gameData.players[i];
        if (bot.panelID == null || bot.id != "bot") {
            continue;
        }

        var min = null;
        var lmin = 100000;
        var p = arrayGetBy(worldData.panels, bot.panelID);
        var ppos = p.position;

        for (var j = 0; j < worldData.dynamics.length; j++) {
            var bpos = worldData.dynamics[j].position;
            var l1 = (bpos[0] - ppos[0]) * (bpos[0] - ppos[0]) + (bpos[1] - ppos[1]) * (bpos[1] - ppos[1]);
            if (l1 < lmin) {
                lmin = l1;
                min = worldData.dynamics[j];
            }
        }
        var vel = min.velocity;
        bpos = min.position;
        var dpos = [bpos[0] - ppos[0], bpos[1] - ppos[1]];

        var d = vel[0] * dpos[0] + vel[1] * dpos[1];

        p.dir = null;
        if (d < 0) {
            var w = ( 0.5 - Math.random()) * (bot.brain);
            dpos[0] *= p.direction[0];
            dpos[1] *= p.direction[1];
            var dlen = ( dpos[0] * dpos[0] + dpos[1] * dpos[1] ) * (1 + w);

            if (dlen > 0.01) {
                p.dir = dpos;
            }
        }
    }
};

game.teamEnd = function (panelID, lost) {

    worldData.dynamics.length = 0;
    var count = gameData.players.length;
    for (i = 0; i < worldData.panels.length; i++) {
        var p = worldData.panels[i];
        p.position = [p.def_pos[0], p.def_pos[1], 0];
        p.dir = null;
        p.moveTo = null;
    }
    var looserID;
    for (var i = 0; i < Math.min(count, 4); i++) {
        if (!gameData.players[i]) {
            continue;
        }
        gameData.players[i].score++;
        if (gameData.players[i].panelID == panelID) {
            gameData.players[i].score = 0;
            var looser = gameData.players[i];
            looserID = looser.id;
            if (count > 4) {
                gameData.players[i] = gameData.players[4];
                gameData.players[i].panelID = looser.panelID;
                gameData.players.splice(4, 1);
                looser.panelID = null;
                if ((!lost && looser.id != "bot") || ( gameData.players.length < 5 && looser.id == "bot")) {
                    gameData.players.push(looser);
                }
            } else {
                if (lost) {
                    if (gameData.players.length > 5) {
                        gameData.players.splice(i, 1);
                    } else {
                        gameData.players.splice(i, 1, game.newBot(looser.panelID));
                    }
                }
            }
        }
    }
    server.sendAll("gameTeamEnd", {players: gameData.players, looser: looser});
};

game.sendPositions = function () {
    var pos = {
        balls: worldData.dynamics,
        panels: worldData.panels
    };
    server.sendAll("updatePositions", pos);
};

var spawnTime = new Date().getTime();

game.spawnBall = function () {
    var len = worldData.dynamics.length;
    var time = new Date().getTime();
    if ((spawnTime + gameData.newBallSpawnTime < time) && (len < gameData.maxBalls)) {
        Math.random();
        var sx = 0.5 - Math.random();
        var sy = 0.5 - Math.random();
        var l = Math.sqrt(sx * sx + sy * sy);
        var ball_conf = worldData.ball_conf;
        var ball = {
            name: "ball_" + len,
            position: ball_conf.def_pos,
            velocity: [sx / l, sy / l],
            type: ball_conf.type,
            radius: ball_conf.radius,
            speed: gameData.ballSpeed
        };
        worldData.dynamics.push(ball);
        spawnTime = new Date().getTime();
    }
};
/// main loop
var t = 0;
var frame = 0;

game.updateMath = function (dt) {
    for (var _n = 0; _n < gameData.mathStep; _n++) {
        for (var j = 0; j < worldData.panels.length; j++) {
            var p = worldData.panels[j];
            if (p.dir != null) {
                var dic = gameData.panelSpeed * dt / gameData.mathStep;
                var pos = p.position;
                var dir = p.dir;
                var moveTo = p.moveTo;
                var lim = worldData.panelsLimites;
                if (dir[0] !== 0) {
                    if (dir[0] > 0 && lim[0] - pos[0] > 0) {
                        pos[0] += dic;
                    }

                    if (dir[0] < 0 && lim[0] + pos[0] > 0) {
                        pos[0] -= dic;
                    }
                } else if (dir[1] !== 0) {
                    if (dir[1] > 0 && lim[1] - pos[1] > 0) {
                        pos[1] += dic;
                    }
                    if (dir[1] < 0 && lim[0] + pos[1] > 0) {
                        pos[1] -= dic;
                    }
                }
                if (moveTo) {
                    if (Math.abs(moveTo[0] - pos[0]) * p.direction[0] < 0.1 && Math.abs(moveTo[1] - pos[1]) * p.direction[1] < 0.1) {
                        p.dir = null;
                    }
                }
            }
        }

        for (var i = 0; i < worldData.dynamics.length; i++) {
            var d_ent = worldData.dynamics[i];
            //statics obj
            for (j = 0; j < worldData.statics.length; j++) {
                var s_ent = worldData.statics[j];
                collider.test(d_ent, s_ent);
            }
            //panel obj
            for (j = 0; j < worldData.panels.length; j++) {
                var p_ent = worldData.panels[j];
                collider.test(d_ent, p_ent);
            }
            // move
            dic = d_ent.speed * dt / gameData.mathStep;
            d_ent.position = [d_ent.position[0] + d_ent.velocity[0] * dic, d_ent.position[1] + d_ent.velocity[1] * dic, 0];

        }
    }
    game.botsUpdate(dt);
};

game.update = function (dt) {

    //server framerate
    /*
     t += dt;
     frame++;
     if (t > 5) {
     console.log("[game] update FPS:" + frame / 5);
     t = 0;
     frame = 0;
     }*/
    // --

    if (gameData.players.length >= 4 && Object.keys(gameData.activeClients).length != 0) {
        game.spawnBall();
        game.updateMath(dt);
        game.sendPositions();
    }
};

// --init

var pTime = new Date().getTime();
var interval = setInterval(
    function () {
        var nTime = new Date().getTime();
        game.update((nTime - pTime) / 1000);
        pTime = nTime;
    }, gameData.timeStep);
