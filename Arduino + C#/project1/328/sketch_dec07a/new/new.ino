
const int PIN_1 = 8;
const int PIN_2 = 9;
const int pulsePIN = 6;
const int pulseDelay = 5;


int T1 = 0; //  включеник
int T2 = 0; // выключение

int N = 0;
int n = 0;

int ADC_pin = 0;
const int maxPin = 5;
int delta_time = 0;
unsigned long startADC_time = 0;
bool readFromADC = false;

unsigned long _time = 0;
unsigned long start_time = 0;

bool isStart = false;

void ReadDataUart(){
  
  int _T1 = 0;
  int _T2 = 0;
  int _N = 0;
  
  if(Serial.available() > 0){
    char c = Serial.read();
    
    if(c=='#'){
      isStart = false;
      
      T1 = Serial.parseInt();
      T2 = Serial.parseInt();
      N = Serial.parseInt();
	  delta_time = (T1 * 2) / 3;
    }
    
    if(c=='E')
    {
      n = 0;
     isStart = false; 
     readFromADC = false;
    }
    
    if(c=='S' && !isStart){
      n=0;
      isStart = true;
      start_time = millis();
      if(readFromADC)
        startADC_time = start_time;
    }
	if(c == 'A' && !readFromADC) {
		int p = Serial.parseInt();
		if(p > maxPin)
			p = maxPin;
		if(p < 0)
			p = 0;
		ADC_pin = p;
		
		readFromADC = true;
		//startADC_time = millis();
	}
	if(c == 'F' && readFromADC) {
		readFromADC = false;
	}
  }
}

unsigned long old_time = 0;

void setup() {
  Serial.begin(9600);
  Serial.write("Hi! I am nano");
  pinMode(PIN_1, OUTPUT);
  pinMode(PIN_2, OUTPUT);
  pinMode(pulsePIN, OUTPUT);
  
  
}

int PIN_1_STATE = LOW;
int PIN_2_STATE = LOW;
int pulse_time = 0;

void PrintADC() {
	int val = analogRead(ADC_pin);

	Serial.print("D ");
	Serial.print(millis() - startADC_time);
	Serial.print(' ');
	Serial.println(val);

}

void loop() {
  ReadDataUart();
  
  if(n < N && isStart){
		unsigned long _time = millis() - start_time;
		
		if(_time < T1 && PIN_1_STATE == LOW) {
			PIN_1_STATE = HIGH;
			digitalWrite(PIN_1, PIN_1_STATE);
		//Serial.println("PIN1:ON");
		}
		
		if(_time >= T1 && _time < T1+ T2 && PIN_1_STATE == HIGH){
			PIN_1_STATE = LOW;
			digitalWrite(PIN_1, PIN_1_STATE);
		  //Serial.println("PIN1:OFF");
		
			pulse_time = millis();
			
		}
		// 2 импульс
		if(_time >= T1+T2 && _time < 2*T1 + T2 && PIN_2_STATE == LOW){
			PIN_2_STATE = HIGH;
			digitalWrite(PIN_2, PIN_2_STATE);
		//Serial.println("PIN2:ON");
	 
		}
		
		if(_time >= 2*T1+T2 && PIN_2_STATE == HIGH){
			PIN_2_STATE = LOW;
			digitalWrite(PIN_2, PIN_2_STATE);
			start_time = millis();
			pulse_time = start_time;
			n++;
		
		//Serial.println("PIN2:OFF");
		
		Serial.println(n,DEC);
		}
		
		if(readFromADC) {
			if(PIN_1_STATE == HIGH && _time >= delta_time) {
				//willRead = true;
				PrintADC();
			}
			
			if(PIN_2_STATE == HIGH && _time >= delta_time + T1 +T2) {
				///willRead = true;
				PrintADC();
			}
		}
	
		if(pulse_time > 0){
			if(millis() - pulse_time >= pulseDelay) {
				digitalWrite(pulsePIN, LOW);
				pulse_time = 0;
			} else {
				digitalWrite(pulsePIN, HIGH);
			}
		}
	
	} else {
		if(isStart) {
		isStart = false;
		readFromADC = false;
    Serial.println("E");
		}
	}
	//n++;
 }

