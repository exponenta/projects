﻿using System;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Threading;

namespace com
{
    public partial class Form1 : Form
    {
        ManualResetEvent done = new ManualResetEvent(false);
        bool started = false;
        bool formClosing = false;

        int T1 = 0;
        int T2 = 0;
        int N = 0;
        // # - установка параметров
        // S  - старт
        // E - выключение счета
        // A - чтение с ацп
        // F - отключение чтения с ацп

        delegate void AddToGrid(object[] obj);
        delegate void WriteStop();
        AddToGrid addTogrid;
        WriteStop writeStoped;

        public Form1()
        {
            InitializeComponent();
            addTogrid = new AddToGrid(AddObjToGrid);
            writeStoped = new WriteStop(ResetAllControls);
        }

        private void ResetAllControls()
        {
            if(this.InvokeRequired) {
                this.Invoke(new Action(resetAll));
            }else
            {
                resetAll();
            }
        }

        private void resetAll()
        {
            started = false;
            button1.Text = "START";
            MessageBox.Show("Чтение завершено!");
        }

        private void AddObjToGrid(object[] obj)
        {
            if (dataGridView1.InvokeRequired)
                dataGridView1.BeginInvoke(new Action(() => { dataGridView1.Rows.Add(obj); }));
            else
                dataGridView1.Rows.Add(obj);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            comLists.Items.AddRange(ports);
            if (ports.Length > 0)
            {
                comLists.SelectedIndex = 0;
                comLists_SelectedIndexChanged(sender, null);
            }

            dataGridView1.Rows.Add(values);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!started && !serialPort1.IsOpen)
                serialPort1.Open();

            if (!started && serialPort1.IsOpen)
            {
                started = true;
                T1 = int.Parse(textBox1.Text);
                T2 = int.Parse(textBox2.Text);
                N = int.Parse(textBox3.Text);

                if (T1 > 0 && T2 > 0 && N > 0)
                {
                    string line = String.Format("# {0} {1} {2}", T1, T2, N);
                    serialPort1.WriteLine(line);
                    if (readFromADC.Checked)
                    {
                        readFromADC_CheckedChanged(readFromADC, null);
                    }
                    serialPort1.WriteLine("A " + ADC_pin.Text);
                    serialPort1.WriteLine("S");
                }
                button1.Text = "END";
            }
            else if (serialPort1.IsOpen)
            {

                serialPort1.WriteLine("E");
                button1.Text = "START";
                started = false;
                serialPort1.Close();
            }


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        private void comLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();

            serialPort1.PortName = SerialPort.GetPortNames()[comLists.SelectedIndex];
            serialPort1.Open();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var dr = saveFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;

                TextWriter tw = new StreamWriter(name);
                if (tw != null)
                {
                    tw.WriteLine("Время\tЧистые c АЦП\tВычисленные");
                    for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    {
                        var r = dataGridView1.Rows[i];
                        string str = string.Format("{0}\t{1}\t{2}", r.Cells[0].Value.ToString(), r.Cells[1].Value.ToString(), r.Cells[2].Value.ToString());
                        tw.WriteLine(str);
                    }
                    tw.Close();
                    MessageBox.Show("Данные сохранены!", "Удача", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {

                    DialogResult dr2 = MessageBox.Show("Ошибка!\n Возможно файл не доступен! Повторить?", "Ошибка", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                    if (dr2 == DialogResult.OK)
                        button3_Click(sender, null);
                }
            }
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (formClosing)
            {
                serialPort1.Close();  // Закрыть порт
                done.Set();  // Дать основному потоку знать о том, что порт свою работу завершил
                return;  // Завершить метод без отправки ответа
            }

            if (!readFromADC.Checked)
            {
                return;
            }

            SerialPort port = (SerialPort)sender;
            string line = "";

            do
            {
                try
                {
                    line = port.ReadLine();
                    if(line[0] == 'E')
                    {
                        Invoke(writeStoped);
                        return;
                    }
                    string[] val = line.Split(' ');
                    if (val.Length == 3)
                    {
                        if (val[0] == "D")
                        {
                            int time = int.Parse(val[1]);
                            int raw = int.Parse(val[2]);
                            float value = raw * float.Parse(mulCoof.Text);

                            object[] values = new object[3] { time, raw, value };
                            //dataGridView1.Rows.Add(values);
                            dataGridView1.Invoke(addTogrid, new object[1] { values });
                        }
                    }
                }
                catch { return; }

            } while (line.Length > 0);
        }

        private void readFromADC_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox b = (CheckBox)sender;

            if (serialPort1.IsOpen)
            {
                if (b.Checked)
                {
                    ADC_pin.Enabled = false;

                    //разрешаем использовать АЦП
                    //serialPort1.WriteLine("A " + ADC_pin);
                }
                else
                {
                    ADC_pin.Enabled = true;
                    //отключаем АЦП
                    //serialPort1.WriteLine("F");
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            formClosing = true;
            if (!done.WaitOne(TimeSpan.FromSeconds(2)))
                serialPort1.Close();
        }
    }
}
