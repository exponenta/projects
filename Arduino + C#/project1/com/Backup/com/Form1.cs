﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace com
{
    public partial class Form1 : Form
    {
        int T1 = 0;
        int T2 = 0;
        int N = 0;

        public Form1()
        {
            

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            comLists.Items.AddRange(ports);
            comLists.SelectedIndex = 0;

            comLists_SelectedIndexChanged(sender, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            T1 = int.Parse(textBox1.Text);
            T2 = int.Parse(textBox2.Text);
            N = int.Parse(textBox3.Text);
            
            if (T1 > 0 && T2 > 0 && N > 0)
                {
                    if (serialPort1.IsOpen)
                    {
                       string line = String.Format("# {0} {1} {2} S", T1, T2, N);
                       serialPort1.WriteLine(line);
                    }
                }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void comLists_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();

             serialPort1.PortName = SerialPort.GetPortNames()[comLists.SelectedIndex];
             serialPort1.Open();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.WriteLine("E");
            }
        }
        
    }
}
