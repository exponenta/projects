/***
 *      ╔═╗┌─┐┌─┐┌─┐┌─┐      
 *      ╚═╗├─┘├─┤│  ├┤       
 *      ╚═╝┴  ┴ ┴└─┘└─┘      
 *    ╦┌┐┌┬  ┬┌─┐┌┬┐┌─┐┬─┐┌─┐
 *    ║│││└┐┌┘├─┤ ││├┤ ├┬┘└─┐
 *    ╩┘└┘ └┘ ┴ ┴─┴┘└─┘┴└─└─┘
 */

#include <curses.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>



#define MAXW 42
#define MAXH 23

const char* logo_text=
 "::::::::::::::::"
 ":     SPACE    :"
 ":   INVADERS   :"
 "::::::::::::::::";

struct Shape
{
	const char *shape;
	int x;
	int y;
	int w;
	int h;
	int param;

	Shape(){
		shape = "# #### #  Y ";
		x=0;
		y=0;
		w = 3;
		h = 4;
		param=0;
	}

	Shape(const char *s,int _w,int _h){
		shape=s;
		w=_w;
		h=_h;
		x=0;
		y=0;
		param=0;
	}

	void draw(){
		for(int i=0;i<w*h;i++){
			//char str[2]={shape[i]};
			if(shape[i]==' ') continue;
			mvaddch(i/w+y,i%w+x,shape[i]);
		}
	}
	bool pointTest(int _x,int _y){
		if(_x-x>=0 && _x-x<w && _y-y>=0 && _y-y<h)
			return shape[(_y-y)*h+(_x-x)]!=' ';
		return false;
	}	
};

struct bullet {
	int x;
	int y;
	int dx;
	int dy;
	bool coll;

	bullet(int _x,int _y){
		x=_x;
		y=_y;
		coll=false;
		dx=0;
		dy=1;
	}
	bullet(){
		x=0;
		y=0;
		coll=false;
		dx=0;
		dy=1;
	}
};


int main()
{
   
   int AIM_SHIPS_COUNT=10;
   //unut random
   srand(time(NULL));

   int x,y;
   x=MAXW/2-1;
   y=2;
   
   int score = 0;
   
   bullet blts[100];
   int blts_count=0;

   int ticks=0;

   Shape logo(logo_text,16,4);
   logo.x = MAXW/2-8;



   //int curses
   initscr();
   noecho(); 
   curs_set(0);
   timeout(0);
   start_color();

   init_pair(1, COLOR_RED, COLOR_BLACK);
   init_pair(2, COLOR_BLUE, COLOR_BLACK);
   init_pair(3, COLOR_GREEN,COLOR_BLACK);
   init_pair(4, COLOR_YELLOW, COLOR_BLACK);
   init_pair(5, COLOR_BLACK,COLOR_GREEN);
   
   //main menu
   int selected = 0;
   const char menu_item[3][9]={"New game","About","Exit"};
   clear();
   
   attron(COLOR_PAIR(4));
   logo.draw();
   while(1){
   		for (int i=0;i<3;i++)
   		{
   			char ar = ' ';
   			if(selected==i){
   				attron(COLOR_PAIR(5));
   				ar='>';
   			}
   			else
   				attron(COLOR_PAIR(3));
   			mvprintw(MAXH/2+i,10,"%c%s",ar,menu_item[i]);
   		}

   		refresh();
    	switch(getchar()){
   			case 'w':
   				selected=selected==0?2:selected-1;
   				break;
   			case 's':
   				selected=(selected+1)%3;
   				break;
   			case ' ':
   				switch(selected){
   					case 0: goto main_loop;break;//goto, umm
   					case 1: goto about;break;
   					case 2: endwin();return 0;
   				}
   		}
   }
   //about 
   about:;
   //main game loop
   main_loop: ;

   Shape ship;
   Shape mships[10];
   Shape wall("|||||||||||||||||||||||||",1,MAXH);

      for(int i=0;i<AIM_SHIPS_COUNT;i++)
   	{
   		mships[i].shape=" A NNNN N";
   		mships[i].w=3;
   		mships[i].h=3;

   		mships[i].x=3+i%5*9;
   		mships[i].y=(MAXH-3)-(i/5)*4;

   	}
   
   bool GAMOVER=false;
   char life_line[20];
   int LIFE=20;
   for (int i = 0; i < 20; ++i)
   		life_line[i]='#';


   while(1) 
   {
   		clear();
   		//top bar and walls
   		attron(COLOR_PAIR(3));
   		
   		mvprintw(0,0,"::  SCORE:%4d  LIFE:%s\n",score,life_line);
   		wall.x=0;
   		wall.draw();
   		wall.x=MAXW;
   		wall.draw();
   		//attron(COLOR_PAIR(2));
		mvprintw(MAXH/2,MAXW/2-4,"Level  %d",1);	

		//main ship
   		attron(COLOR_PAIR(1));
   		ship.draw();

		//aim ships 
		
		attron(COLOR_PAIR(2));
		int fire_ship = rand()%(AIM_SHIPS_COUNT+1)-1;
		for(int i=0;i<AIM_SHIPS_COUNT;i++){
			mships[i].draw();
			if(ticks%100==0 && fire_ship==i)
			{
				blts[blts_count] = bullet(mships[i].x+1,mships[i].y-1);	
				blts[blts_count].dy = -1;
				blts_count++;	
			}
		}

		//bullet update and draw	
		attron(COLOR_PAIR(4));
		//draw allways
		for (int i=0;i<blts_count;i++)
			mvaddch(blts[i].y,blts[i].x,'*');	
		
		//but update every 10 ticks
		if(ticks%10==0){
			for (int i=0;i<blts_count;i++)
			{
				//but update every 10 ticks
				blts[i].y+=blts[i].dy;
				//test bullet colliding 
				for (int l=0;l<AIM_SHIPS_COUNT;l++)
				{
					//if(mships[l].param==1)
					//	continue;
					if(mships[l].pointTest(blts[i].x,blts[i].y))
						{
							blts[i].coll=true;
							//mships[l].param=1;
							for(int i=l;i<AIM_SHIPS_COUNT-1;i++)
								mships[i]=mships[i+1];
							AIM_SHIPS_COUNT--;
							score++;
						}
				}
				if(ship.pointTest(blts[i].x,blts[i].y))
				{
					//change life line
					if(LIFE<20)
						life_line[LIFE]='#';
					LIFE--;
					life_line[LIFE]=0;
					blts[i].coll=true;
					GAMOVER=(LIFE<=0);

				}
					//destroying bullet
				if(blts[i].y>30 || blts[i].coll || blts[i].y<2){
					for(int l=i;l<blts_count-1;l++)
						blts[l]=blts[l+1]; //and shifting
					blts_count--;
				}
			}
		}
		
		//ship updating
		switch(getch()){
			case 'a':
				x=x>1?x-1:1;
				break;
			case 'd':
				x=x<(MAXW-3)?x+1:MAXW-3;
				break;
			case ' ':
				blts[blts_count]=bullet(x+1,y+4);
				blts_count++;
				break;
		}
		ship.x = x;
		ship.y = y;
		//sleep
		refresh();
		usleep(8000);
		ticks++;
   }
   endwin();
}