package ru.exponenta.recorder;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;


import android.os.Handler;
import android.os.SystemClock;
import android.text.InputType;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class main extends Activity {

	static {
		System.loadLibrary("mp3lame");
	}

	private native void initEncoder(int numChannels, int sampleRate, int bitRate, int mode, int quality);

	private native void destroyEncoder();

	private native int encodeFile(String sourcePath, String targetPath);

	public static final int NUM_CHANNELS = 1;
	public static final int SAMPLE_RATE = 16000;
	public static final int BITRATE = 128;
	public static final int MODE = 1;
	public static final int QUALITY = 2;

    private AudioRecord mRecorder;
	private short[] mBuffer;

	private File mRawFile;
	private File mEncodedFile;

    private Button stop_b;
    private Button start_b;
    private Button send_b;
    private TextView hints;

    private TextView time;
    private long startTime=0;
    private long pauseTime=0;
    private long  tickTime=0;

    private boolean isRecording = false;
    private boolean isPause =false;
    //Thead pause solution
    //http://stackoverflow.com/questions/6776327/how-to-pause-resume-thread-in-android
    private final Object pauseObj = new Object();
    private Handler uiThHannd = new Handler();

    private String mail;

    public void onStartPause(View view) {

        if(isRecording){

            if(!isPause){

                synchronized (pauseObj) {
                    isPause=true;
                }
                pauseTime=SystemClock.uptimeMillis();
            }else {

                synchronized (pauseObj) {
                   isPause=false;
                   pauseObj.notifyAll();
                }
                startTime+=SystemClock.uptimeMillis()-pauseTime;

            }

        }else{

            isRecording=true;

            Time time = new Time();
            time.setToNow();
            String name =  Environment.getExternalStorageDirectory()+"/"+time.format("%Y%m%d%H%M%S");

            mRawFile = new File(name);
            mEncodedFile = new File(name+".mp3");

            startTime = SystemClock.uptimeMillis();
            mRecorder.startRecording();
            recordThead().start();
        }

        updView();
    }

    public void onStop(View view){

        tickTime=0;
        synchronized (pauseObj) {
            isPause=false;
            isRecording=false;
            pauseObj.notifyAll();
        }

        mRecorder.stop();

        int result = encodeFile(mRawFile.getAbsolutePath(), mEncodedFile.getAbsolutePath());
        if (result == 0) {
            Toast.makeText(main.this, "Сохранил в " + mEncodedFile.getName(), Toast.LENGTH_SHORT)
                    .show();
        }

        mRawFile.delete();

        updView();
    }

    private void sendTo(){

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("audio/mpeg");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{mail});
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///"+mEncodedFile.getAbsolutePath()));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Lame");
        startActivity(Intent.createChooser(emailIntent , "Отправить.."));

    }

    public void onSend(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.inputTitle);

        final EditText input = new EditText(this);

        input.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        builder.setView(input);

        builder.setPositiveButton(R.string.next, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mail = input.getText().toString();
                sendTo();
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mail = null;
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void findView(){

        start_b = (Button) findViewById(R.id.start_b);
        stop_b = (Button) findViewById(R.id.stop_b);
        send_b = (Button) findViewById(R.id.send_b);
        hints = (TextView) findViewById(R.id.time_hint);
        time = (TextView) findViewById(R.id.timer);
    }

    private void updView()
    {

        time.setText(formatTime(tickTime));

        stop_b.setEnabled(isRecording && mEncodedFile!=null);
        if(mEncodedFile!=null)
            send_b.setEnabled(!isRecording && mEncodedFile.exists());
        else
            send_b.setEnabled(false);

        if(isRecording) {
            if (isPause) {
                start_b.setText(R.string.rewind);
                hints.setText(R.string.hint_pause);
            } else {
                start_b.setText(R.string.pause);
                hints.setText(R.string.hint_rec);
            }
        }else{
            hints.setText(R.string.hint_nothing);
            start_b.setText(R.string.record);
        }

    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        findView();
        updView();
        initRecorder();
		initEncoder(NUM_CHANNELS, SAMPLE_RATE, BITRATE, MODE, QUALITY);

	}

    //исправляем глюк с поворотом
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setContentView(R.layout.activity_main);
        //и обновляем View'ы
        findView();
        updView();
    }

	@Override
	public void onDestroy() {
		mRecorder.release();
		destroyEncoder();
		super.onDestroy();
	}

	private void initRecorder() {
		int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
		mBuffer = new short[bufferSize];
		mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
				AudioFormat.ENCODING_PCM_16BIT, bufferSize);
	}

    private String formatTime(long time)
    {
        int secs = (int) (time / 1000);
        int mins = secs / 60;
        secs = secs % 60;
        int milliseconds = (int) (time % 1000);
        return String.format("%02d:%02d:%03d",mins,secs,milliseconds);
    }

    private Thread recordThead(){
        return new Thread(new Runnable() {
            @Override
            public void run()
            {
                DataOutputStream output;

                try {
                    output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(mRawFile)));
                    while (isRecording) {
                        //set record time in UI thread
                        tickTime = SystemClock.uptimeMillis()-startTime;

                        uiThHannd.post(new Runnable() {
                                           @Override
                                           public void run() {
                                               time.setText(formatTime(tickTime));
                                           }
                                       });

                        synchronized (pauseObj) {
                            while (isPause) {

                                try {
                                    pauseObj.wait();
                                } catch (InterruptedException e) {
                                }
                            }
                        }

                        int readSize = mRecorder.read(mBuffer, 0, mBuffer.length);
                        for (int i = 0; i < readSize; i++) {
                            output.writeShort(mBuffer[i]);
                        }

                    }

                    output.flush();
                    output.close();

                } catch (IOException e) {
                    Toast.makeText(main.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

        });
    }

}
