package ru.exponenta.locator;

import android.app.Activity;

import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class red extends Activity  implements LocationListener, GpsStatus.NmeaListener {

    static Location pos;
    static String nmea;
    private float error;
    private LocationManager LM;

    public void Compare()
    {
        TextView t = (TextView) findViewById(R.id.loc);

        if(pos!=null && nmea!=null && error>0){

            double loc_lat =  pos.getLatitude();
            double loc_long =  pos.getLongitude();

            String[] str = nmea.split(",");

            Log.d("Raw",nmea);
            for(int i=0;i<str.length;i++)
                Log.d("Nmea:",str[i]);


            double nmea_lat = 0.0;

            if(str[2].length()>2) {
                nmea_lat = Double.parseDouble(str[2].substring(0,2))
                +Double.parseDouble(str[2].substring(2))/60;
                if (str[3].charAt(0) == 'S') {
                    nmea_lat = -nmea_lat;
                }
            }

            double nmea_long = 0.0;

            if(str[4].length()>2) {
                nmea_long = Double.parseDouble(str[4].substring(0,3))+
                        Double.parseDouble(str[4].substring(3))/60;

                if (str[5].charAt(0) == 'W') {
                    nmea_long = -nmea_long;
                }
            }

            if(t!=null)
                t.setText("Nmea:\n\t"+nmea_lat+","+nmea_long+"\nLocation:\n\t"+loc_lat+","+loc_long);

            if(nmea_long!=0.0 && nmea_lat!=0.0){
                    //хз как сравнивать
                double d_l = Math.abs(nmea_long-loc_long);
                double d_a = Math.abs(nmea_lat-loc_lat);
                if(Math.sqrt(d_l*d_l+d_a*d_a)>error)
                    onActivate();
                else
                    onDeactivate();
            }
        }
    }

    public void onDeactivate()
    {
        ImageView red_Panel = (ImageView) findViewById(R.id.redPanel);
        if(red_Panel!=null)
            red_Panel.setAlpha((float)0.0);
    }


    public void onActivate(){
        ImageView red_Panel = (ImageView) findViewById(R.id.redPanel);
        if(red_Panel!=null)
            red_Panel.setAlpha((float)1.0);

        NotificationManager n = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification note = new Notification();
        note.defaults = Notification.DEFAULT_SOUND;
        n.notify(1,note);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_red);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        error = getIntent().getFloatExtra("error",0);
        //Location listener
        LM = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LM.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);

        pos = LM.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        LM.addNmeaListener(this);
    }

    @Override
    public void onLocationChanged(Location loc) {
        pos = loc;
        Compare();
    }

    @Override
    public void onNmeaReceived(long timestamp, String n) {


        if(n.startsWith("$GPGGA")) //NMEA спецификация

        {
            Compare();
            nmea = n;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.red, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_red, container, false);
            return rootView;
        }
    }

    @Override
    public void onBackPressed(){
        //kill all listener
        LM.removeUpdates(this);
        LM.removeNmeaListener(this);
        LM=null;

        super.onBackPressed();
        finish();
    }

    @Override
    public void onProviderDisabled(String provider) {}
    @Override
    public void onProviderEnabled(String provider) {}
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
